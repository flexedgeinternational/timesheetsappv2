package com.example.view;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.localization.Localizer;
import com.example.model.Role;
import com.example.service.crud.RoleService;
import com.example.vaadin.core.view.AbstractListView;
import com.example.vaadin.core.window.GenericEditWindow;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
@SpringView(name = "RoleListView")
public class RoleListView extends AbstractListView<Role> implements View {

	@Autowired
	RoleService customService;

	@Override
	public void enter(ViewChangeEvent event) {

	}

	@Override
	protected void initService() {
		this.service = customService;
	}

	@Override
	protected String getViewTitle() {
		return Localizer.getLocalizedTerm("Roles");
	}

	@Override
	protected Object[] visibleColumns() {
		return new Object[] { "name" };
	}

	@Override
	protected Class<Role> getEntityClass() {
		return Role.class;
	}

	@Override
	protected void initWindowEditFields(GenericEditWindow window) {
		TextField name = new TextField(Localizer.getLocalizedTerm("name"));
		name.setNullRepresentation("");
		window.addField(name, "name");

	}

	@Override
	protected void initFooterContent() {

	}
}
