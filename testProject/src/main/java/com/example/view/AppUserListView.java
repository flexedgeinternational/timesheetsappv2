package com.example.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.example.localization.Localizer;
import com.example.model.AppUser;
import com.example.service.crud.AppUserService;
import com.example.service.crud.RoleService;
import com.example.vaadin.core.view.AbstractListView;
import com.example.vaadin.core.window.GenericEditWindow;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;

@SuppressWarnings("serial")
@SpringView(name = "AppUserListView")
public class AppUserListView extends AbstractListView<AppUser> implements View {

	@Autowired
	@Qualifier("appUserService")
	AppUserService customService;

	@Autowired
	RoleService roleService;

	@Override
	public void enter(ViewChangeEvent event) {
	}
	
	@Override
	protected void initService() {
		this.service = customService;
	}

	@Override
	protected String getViewTitle() {
		return Localizer.getLocalizedTerm("AppUsers");
	}

	@Override
	protected Object[] visibleColumns() {
		return new Object[] { "username", "firstname", "lastname", "email" };
	}

	@Override
	protected Class<AppUser> getEntityClass() {
		return AppUser.class;
	}

	@Override
	protected void initWindowEditFields(GenericEditWindow window) {
	}

	@Override
	protected boolean addEditInWindow() {
		return false;
	}

	@Override
	protected void initFooterContent() {

	}


}
