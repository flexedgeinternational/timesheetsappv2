package com.example.view;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.localization.Localizer;
import com.example.model.Employee;
import com.example.service.crud.EmployeeService;
import com.example.vaadin.core.view.AbstractListView;
import com.example.vaadin.core.window.GenericEditWindow;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
@SpringView(name = "EmployeeListView")
public class EmployeeListView extends AbstractListView<Employee> implements View {

	@Autowired
	EmployeeService customService;

	@Override
	public void enter(ViewChangeEvent event) {

	}
	
	@Override
	protected void initService() {
		this.service = customService;
	}

	@Override
	protected String getViewTitle() {
		return Localizer.getLocalizedTerm("ManageEmployees");
	}

	@Override
	protected Object[] visibleColumns() {
		return new Object[] { "firstname", "lastname" };
	}

	@Override
	protected Class<Employee> getEntityClass() {
		return Employee.class;
	}

	@Override
	protected void initWindowEditFields(GenericEditWindow window) {
		TextField firstname = new TextField("First Name");
		firstname.setNullRepresentation("");
		window.addField(firstname, "firstname");

		TextField lastname = new TextField("Last Name");
		lastname.setNullRepresentation("");
		window.addField(lastname, "lastname");

		TextField age = new TextField("Age");
		age.setNullRepresentation("");
		window.addField(age, "age");
	}

	@Override
	protected boolean addEditInWindow() {
		return true;
	}
	
	@Override
	protected void initFooterContent() {

	}

}
