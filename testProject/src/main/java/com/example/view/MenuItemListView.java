package com.example.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.localization.Localizer;
import com.example.model.MenuItem;
import com.example.model.MenuItemRole;
import com.example.model.Role;
import com.example.service.crud.RoleService;
import com.example.service.util.MenuService;
import com.example.vaadin.component.GridTree;
import com.example.vaadin.core.view.AbstractListView;
import com.example.window.SelectedMenuWindow;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid.MultiSelectionModel;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

@SpringView(name = "MenuItemListView")
public class MenuItemListView extends AbstractListView<MenuItem> implements View {

	@Autowired
	MenuService menuService;
	@Autowired
	RoleService roleService;

	private GridTree listGrid;
	private List<CheckBox> rolesCheck = new ArrayList<>();
	private Button selectMenu;
	private List<MenuItem> menuItems = new ArrayList<>();
	protected final BeanFieldGroup<MenuItem> binder = new BeanFieldGroup<>(MenuItem.class);

	@Override
	public void enter(ViewChangeEvent event) {
	}

	@Override
	protected Object[] visibleColumns() {
		return new Object[] { "menuTitle" };
	}

	@Override
	protected void initService() {
		this.service = menuService;
	}

	@Override
	protected String getViewTitle() {
		return Localizer.getLocalizedTerm("MenuManager");
	}

	@Override
	protected Class<MenuItem> getEntityClass() {
		return MenuItem.class;
	}

	@Override
	protected boolean addEditInWindow() {
		return false;
	}

	@Override
	protected void initContent() {
		super.initContent();
		selectMenu = new Button(Localizer.getLocalizedTerm("SelectMenu"));
		actionsLayout.addComponent(selectMenu);
		selectMenu.setIcon(FontAwesome.CHECK);
		selectMenu.setVisible(false);
		selectMenu.addClickListener(e -> selectMenuAction());
		actionsLayout.setComponentAlignment(selectMenu, Alignment.TOP_RIGHT);
	};

	public void generateCheckRoles() {
		HorizontalLayout roleLayout = new HorizontalLayout();
		roleLayout.setSpacing(true);
		roleLayout.setMargin(true);
		Responsive.makeResponsive(roleLayout);
		Label filterlabel = new Label(Localizer.getLocalizedTerm("Display.Menus.for.Role"));
		filterlabel.setSizeUndefined();
		filterlabel.setStyleName(ValoTheme.LABEL_BOLD);
		// filterlabel.setStyleName("label-filter");
		roleLayout.addComponent(filterlabel);
		GridLayout filterCriteria = new GridLayout();
		filterCriteria.setSpacing(true);
		filterCriteria.setColumns(5);
		// filterCriteria.setStyleName("filterCriteria");
		List<Role> roles = roleService.getListObjects();
		Collections.sort(roles, new Comparator<Role>() {
			@Override
			public int compare(Role o1, Role o2) {
				return o1.getName().compareTo(o2.getName());
			}

		});
		CheckBox c;
		for (Role r : roles) {
			c = new CheckBox(r.getName());
			c.setStyleName(ValoTheme.CHECKBOX_SMALL);
			rolesCheck.add(c);
			c.setImmediate(true);
			filterCriteria.addComponent(c);
			initialiseCheckBoxesListner(c);
		}

		roleLayout.addComponent(filterCriteria);
		roleLayout.setComponentAlignment(filterlabel, Alignment.TOP_LEFT);
		addComponent(roleLayout);
	}

	private void initialiseCheckBoxesListner(CheckBox c) {
		c.addValueChangeListener(new ValueChangeListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (checkFieldsVisibility() == 1) {
					selectMenu.setVisible(true);
					if (c.getValue()) {
						menuItems = menuService.findByRole(c.getCaption());
					} else {
						// find checkBox with value true

						if (checkedOne() != null) {
							menuItems = menuService.findByRole(checkedOne().getCaption());
						}

					}
					reloadGridData();
				} else if (checkFieldsVisibility() > 1) {
					menuItems = new ArrayList<>();
					selectMenu.setVisible(false);
					Notification.show(Localizer.getLocalizedTerm("only.one.role.must.be.checked") + " !!!");
					reloadGridData();
				} else if (checkFieldsVisibility() == 0) {
					selectMenu.setVisible(false);
					menuItems = new ArrayList<>();
					reloadGridData();
				}
			}
		});
	}

	public CheckBox checkedOne() {
		for (CheckBox c : rolesCheck) {
			if (c.getValue()) {
				return c;
			}
		}
		return null;
	}

	public int checkFieldsVisibility() {
		int nb = 0;
		for (CheckBox c : rolesCheck) {
			if (c.getValue())
				nb++;
		}
		return nb;
	}

	@Override
	protected void initGrid() {
		generateCheckRoles();
		listGrid = new GridTree();
		// Activate multi selection mode
		listGrid.setSelectionMode(SelectionMode.MULTI);
		listGrid.setSizeFull();
		reloadGridData();
		listGrid.addSelectionListener(e -> {
			Integer selectedRowSize = listGrid.getSelectedRows().size();
			deleteButton.setEnabled(selectedRowSize > 0);
			editButton.setEnabled(selectedRowSize == 1);
		});

		contentLayout.addComponent(listGrid);
	}

	@Override
	protected void reloadGridData() {
		HierarchicalContainer container = createHierarchicalContent(menuItems);
		listGrid.setContainerDataSource(container);
	};

	@Override
	protected void initDeleteButton() {
		deleteButton = new Button(Localizer.getLocalizedTerm("Delete"), FontAwesome.TRASH);
		actionsLayout.addComponent(deleteButton);
		deleteButton.setEnabled(false);
		deleteButton.addClickListener(e -> {
			Collection<Object> selectedRows = listGrid.getSelectedRows();
			if (!(selectedRows == null || selectedRows.isEmpty())) {
				menuService.deleteSelectedRowsFromCurrentMenuItems(selectedRows,
						roleService.findAllByName(checkedOne().getCaption()).get(0).getOid());
				menuItems = menuService.findByRole(checkedOne().getCaption());
				reloadGridData();
			}
			((MultiSelectionModel) listGrid.getSelectionModel()).deselectAll();
		});

	}

	@Override
	protected void initEditButton() {
		editButton = new Button(Localizer.getLocalizedTerm("Edit"), FontAwesome.EDIT);
		actionsLayout.addComponent(editButton);
		editButton.setEnabled(false);
		editButton.addClickListener(e -> {

			Collection<Object> selectedRows = listGrid.getSelectedRows();
			if (selectedRows.size() == 1) {
				MenuItem row = (MenuItem) selectedRows.iterator().next();
				navigator.navigateTo(getEditViewName() + "/" + service.getSelectedRowId(row));

			}
		});
	}

	private void selectMenuAction() {
		Role selectedRole = roleService.findAllByName(checkedOne().getCaption()).get(0);
		SelectedMenuWindow window = new SelectedMenuWindow(null, binder, Localizer.getLocalizedTerm("SelectMenuItem"),
				menuService.findMenuItemByRoleAndNotInList(menuService.findByRole(checkedOne().getCaption())));

		window.setCancelButtonListener(eventCancel -> closeSelectMenuItemWindow(window));
		window.setSaveButtonListener(eventSave -> saveSelectMenuItemInWindow(window, selectedRole));
		UI.getCurrent().addWindow(window);
	}

	private void saveSelectMenuItemInWindow(SelectedMenuWindow window, Role selectedRole) {

		for (CheckBox check : window.menuItemCheckList) {

			MenuItemRole menuItemRole = null;
			if (check.getValue()) {
				menuItemRole = new MenuItemRole();
				menuItemRole.setRole(selectedRole);
				menuItemRole.setMenuItem(menuService.findByTitle(check.getCaption()));
				menuService.saveMenuItemRole(menuItemRole);

			}
		}
		menuItems = menuService.findByRole(checkedOne().getCaption());
		window.close();
		reloadGridData();
	}

	private void closeSelectMenuItemWindow(SelectedMenuWindow window) {
		binder.discard();
		window.close();
	}

	// Create HierarchicalContainer for GridTree
	public HierarchicalContainer createHierarchicalContent(List<MenuItem> menuItems) {

		HierarchicalContainer container = new HierarchicalContainer();
		// A property that holds the caption is needed for
		// ITEM_CAPTION_MODE_PROPERTY
		container.addContainerProperty(Localizer.getLocalizedTerm("Menus"), MenuItem.class, null);
		new Object() {
			@SuppressWarnings("unchecked")
			public void put(Object[] data, Object parent, HierarchicalContainer container) {
				for (int i = 1; i < data.length; i++) {
					if (data[i].getClass() == MenuItem.class) {
						// Support both ITEM_CAPTION_MODE_ID and
						// ITEM_CAPTION_MODE_PROPERTY
						container.addItem(data[i]);
						container.getItem(data[i]).getItemProperty(Localizer.getLocalizedTerm("Menus"))
								.setValue(data[i]);
						container.setParent(data[i], parent);
						container.setChildrenAllowed(data[i], false);

					} else {// It's an Object[]
						Object[] sub = (Object[]) data[i];
						MenuItem name = (MenuItem) sub[0];

						// Support both ITEM_CAPTION_MODE_ID and
						// ITEM_CAPTION_MODE_PROPERTY
						container.addItem(name);
						container.getItem(name).getItemProperty(Localizer.getLocalizedTerm("Menus")).setValue(name);
						put(sub, name, container);
						container.setParent(name, parent);
					}
				}
			}
		}.put(menuService.getSpecificMenuItem(menuItems), null, container);
		return container;
	}

	@Override
	protected void initFooterContent() {

	}

}
