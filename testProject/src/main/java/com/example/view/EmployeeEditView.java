package com.example.view;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.localization.Localizer;
import com.example.model.Employee;
import com.example.service.crud.EmployeeService;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
@SpringView(name = "EmployeeEditView")
public class EmployeeEditView extends com.example.vaadin.core.view.AbstractEditView<Employee> {

	@PropertyId("firstname")
	private TextField firstname;

	@PropertyId("lastname")
	private TextField lastname;

	@PropertyId("age")
	private TextField age;

	@Autowired
	EmployeeService customService;

	@Override
	protected void initService() {
		this.service = customService;
	}

	@Override
	protected void initFormFields() {
		firstname = new TextField(Localizer.getLocalizedTerm("firstname"));
		firstname.setNullRepresentation("");

		lastname = new TextField(Localizer.getLocalizedTerm("lastname"));
		lastname.setNullRepresentation("");

		age = new TextField(Localizer.getLocalizedTerm("age"));
		age.setNullRepresentation("");

		binder.bind(firstname, "firstname");
		binder.bind(lastname, "lastname");
		binder.bind(age, "age");
		formLayout.setMargin(true);
		formLayout.setSpacing(true);
		formLayout.addComponents(firstname, lastname, age);
	}

	@Override
	protected Class<Employee> getEntityClass() {
		return Employee.class;
	}

}
