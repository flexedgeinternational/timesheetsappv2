package com.example.view;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.localization.Localizer;
import com.example.model.MenuItem;
import com.example.model.MenuItemRole;
import com.example.model.Role;
import com.example.service.crud.RoleService;
import com.example.service.util.MenuService;
import com.example.vaadin.core.view.AbstractEditView;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;

@SpringView(name = "MenuItemEditView")
public class MenuItemEditView extends AbstractEditView<MenuItem> {

	@Autowired
	MenuService menuService;

	@Autowired
	RoleService roleService;

	private static final long serialVersionUID = 1L;

	private FormLayout contentLayout;
	@PropertyId("menuTitle")
	private TextField menuTitle;
	@PropertyId("menuViewName")
	private TextField menuViewName;
	@PropertyId("sortOrder")
	private TextField sortOrder;
	@PropertyId("faIconName")
	private ComboBox faIconName;

	@PropertyId("parentMenuItem")
	private ComboBox parentMenuItem;

	private CheckBox role;

	private GridLayout roleLayout;
	private List<CheckBox> rolesCheck;

	@Override
	protected void initService() {
		this.service = menuService;
	}

	@Override
	protected void initFormFields() {
		contentLayout = new FormLayout();
		contentLayout.setSpacing(true);
		contentLayout.setMargin(true);
		menuTitle = new TextField(Localizer.getLocalizedTerm("menuTitle"));
		menuTitle.setNullRepresentation("");
		contentLayout.addComponent(menuTitle);
		menuViewName = new TextField(Localizer.getLocalizedTerm("menuViewName"));
		menuViewName.setNullRepresentation("");
		contentLayout.addComponent(menuViewName);
		sortOrder = new TextField(Localizer.getLocalizedTerm("sortOrder"));
		sortOrder.setNullRepresentation("");
		contentLayout.addComponent(sortOrder);
		faIconName = new ComboBox(Localizer.getLocalizedTerm("faIconName"));
		faIconName.addItems(FontAwesome.values());
		contentLayout.addComponent(faIconName);
		parentMenuItem = new ComboBox(Localizer.getLocalizedTerm("parentMenu"));
		parentMenuItem.addItems(menuService.getListObjects());
		contentLayout.addComponent(parentMenuItem);
		addComponent(contentLayout);
		roleLayout = new GridLayout();
		roleLayout.setSpacing(true);
		roleLayout.setColumns(2);
		rolesCheck = new ArrayList<>();
		for (Role r : roleService.getListObjects()) {
			role = new CheckBox(r.getName());
			role.addStyleName("role-check");
			roleLayout.addComponent(role);
			rolesCheck.add(role);
		}
		contentLayout.addComponent(roleLayout);
		formLayout.addComponent(contentLayout);

		binder.bind(menuTitle, "menuTitle");
		binder.bind(menuViewName, "menuViewName");
		binder.bind(sortOrder, "sortOrder");
		binder.bind(parentMenuItem, "parentMenuItem");
	}

	@Override
	protected Class<MenuItem> getEntityClass() {
		return MenuItem.class;
	}

	@Override
	protected void saveListner(com.example.service.crud.EntityService<MenuItem> service,
			com.example.vaadin.core.window.GenericEditWindow window,
			com.vaadin.data.fieldgroup.BeanFieldGroup<MenuItem> binder, MenuItem currentObject, String getViewTitle,
			boolean editWindow) {
		super.saveListner(service, null, binder, currentObject, getViewTitle, false);

		if (faIconName.getValue() != null) {
			currentObject.setFaIconName("" + faIconName.getValue());
		} else {
			currentObject.setFaIconName(null);
		}
		if (sortOrder != null && !sortOrder.getValue().equals("")) {
			currentObject.setSortOrder(Integer.valueOf(sortOrder.getValue()));
		}

		for (CheckBox c : rolesCheck) {
			MenuItemRole menuItemRole = new MenuItemRole();
			if (c.getValue()) {
				menuItemRole.setMenuItem(currentObject);
				menuItemRole.setRole(roleService.findAllByName(c.getCaption()).get(0));
				menuService.saveMenuItemRole(menuItemRole);
			}
		}
		service.saveObject(currentObject);
	};

	@Override
	protected void updateEditActionVisibility(MenuItem currentObject, String id) {
		super.updateEditActionVisibility(currentObject, id);
		if (!id.equals("")) {
			if (currentObject.getFaIconName() != null) {
				faIconName.setValue(FontAwesome.valueOf(currentObject.getFaIconName()));
			}
			for (Object[] r : menuService.findRolesByMenuItem(currentObject)) {
				if (getCheckBoxFromCaption(r[1].toString(), rolesCheck) != null) {
					getCheckBoxFromCaption(r[1].toString(), rolesCheck).setValue(true);
				}
			}
		}
	}

	public CheckBox getCheckBoxFromCaption(String caption, List<CheckBox> checkBoxList) {
		CheckBox result = null;
		for (CheckBox c : checkBoxList)
			if (c.getCaption().equals(caption)) {
				result = c;
				break;
			}
		return result;
	}
}
