package com.example.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.GridLayout;

@SuppressWarnings("serial")
@SpringView(name = "HomeView")
public class HomeView extends GridLayout implements View {

	@Override
	public void enter(ViewChangeEvent event) {
	}

}
