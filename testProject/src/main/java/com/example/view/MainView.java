package com.example.view;

import com.example.service.util.MenuService;
import com.example.vaadin.component.DashboardMenu;
import com.vaadin.ui.HorizontalLayout;

@SuppressWarnings("serial")
public class MainView extends HorizontalLayout {

	public MainView(MenuService menuService) {
		setSizeFull();
		addStyleName("mainview");
		addComponent(new DashboardMenu(menuService));
	}

}
