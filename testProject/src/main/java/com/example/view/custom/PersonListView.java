package com.example.view.custom;

import org.springframework.beans.factory.annotation.Autowired;
import com.example.localization.Lang;
import com.example.model.Company;
import com.example.model.Person;
import com.example.service.crud.CompanyService;
import com.example.service.crud.EntityService;
import com.example.service.crud.PersonService;
import com.example.vaadin.component.CustomSelect;
import com.example.vaadin.core.view.AbstractListView;
import com.example.vaadin.core.window.GenericEditWindow;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
@SpringView(name = "PersonListView")
public class PersonListView  extends AbstractListView<Person> implements View {

	@Autowired
	PersonService personService;
	
	@Override
	public void enter(ViewChangeEvent event) {	}

	@Override
	protected Object[] visibleColumns() {
		return new Object[] { "first_name", "last_name" };
	}
	
	@Override
	protected boolean addEditInWindow() {
		return false;
	}
	
	@Override
	protected void initService() {
		this.service = personService;
	}

	@Override
	protected void initFooterContent() {
		Button cancelButton = new Button(Lang.t("back"));
		cancelButton.setIcon(FontAwesome.BACKWARD);
		cancelButton.addClickListener(e -> {
			navigator.navigateTo("VendorClientView");
		});
		
		footerLayout.addComponent(cancelButton);
	}

	@Override
	protected String getViewTitle() {
		return Lang.t("client.contacts");
	}

	@Override
	protected Class<Person> getEntityClass() {
		return Person.class;
	}
}
