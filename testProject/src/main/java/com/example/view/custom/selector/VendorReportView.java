package com.example.view.custom.selector;

import java.util.ArrayList;

import com.example.localization.Lang;
import com.example.vaadin.core.view.AbstractCircleSelectorView;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;

@SuppressWarnings("serial")
@SpringView(name = "VendorReportView")
public class VendorReportView extends AbstractCircleSelectorView<Object> implements View {

	@Override
	protected ArrayList<Component> options() {

		list.add(buildNavButton(
			"Home", "VendorHomeView"
		));
		list.add(buildNavButton(
			"Client Reports", "VendorClientReportsView"
		));
		list.add(buildNavButton(
			"Resource Reports", ""
		));
		
		return list;
		
	}

	@Override
	protected String getListViewName() {
		return Lang.t("reports");
	}


}
