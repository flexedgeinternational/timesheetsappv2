
package com.example.view.custom.graph;


import java.text.NumberFormat;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.graph.ContractLength;
import com.example.service.crud.ContractService;
import com.example.util.GetResource;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.shared.ui.slider.SliderOrientation;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Slider;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class ContractLengthView extends VerticalLayout {

	@Autowired
	ContractService contractService;
		
	private final int minSliderVal = 1000;
	private final int maxSliderVal = 1000000;
	private final double defaultSliderVal = 500000.00;
	
	private DateField date;
	
	private ContractLength contractLengthChart;
	
	int totalMonths;
	int totalCost;
	Long contractId;
	
	public ContractLengthView(int contractId, int totalMonths, int totalCost) {
		
		this.contractId = Long.valueOf(contractId);
		this.totalMonths = totalMonths;
		this.totalCost = totalCost;
		
		addComponent(buildDateField());
		addComponent(buildValueSlider()); 
		contractLengthChart = 
			new ContractLength(
				this.contractId, 
				(int)defaultSliderVal, 
				totalCost,
				totalMonths
			);
		addComponent(contractLengthChart);
		
	}

	private Component buildValueSlider() {
		
		VerticalLayout hLayout = new VerticalLayout();
		hLayout.setWidth("100%");
		
		final Slider valueSlider = new Slider(minSliderVal, maxSliderVal);
		
		
		valueSlider.setCaptionAsHtml(true);
		//valueSlider.setCaption("How long with the contract last with a monthly expenditure of ...");
		
		valueSlider.setValue(defaultSliderVal);
		valueSlider.setOrientation(SliderOrientation.HORIZONTAL);
		valueSlider.setWidth("100%");
		
		Label sliderValue = new Label("$" + Integer.toString(minSliderVal));
		
		valueSlider.addValueChangeListener(
		    new Property.ValueChangeListener() {
		    	     
		    public void valueChange(ValueChangeEvent event) {
		    	
		    	String sliderVal = formatDouble(valueSlider.getValue());
		    	
		        sliderValue.setValue(sliderVal);
		        contractLengthChart.Update(contractId, valueSlider.getValue().intValue());
		        date.setValue(
	        		(Date)GetResource.addMonths(new java.util.Date(), contractLengthChart.totalMonths, "date")
        		);
		        setDateCaption(sliderVal);
	    	} 
		});

		valueSlider.setImmediate(true);

		hLayout.addComponent(valueSlider);
		hLayout.addComponent(sliderValue);
		
		return hLayout;
		
	}
	
	private void setDateCaption(String val) {
		date.setCaption("Expected date the contract will end given a monthly expenditure of " + val);
	}
	
	private String formatDouble(Double doubleVal) {
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		return formatter.format(doubleVal); 
	}
	
	private Component buildDateField() {
		date = new DateField("Expected End Date");
		date.setDateFormat("yyyy-MM-dd");
		date.setValue(
    		(Date)GetResource.addMonths(new java.util.Date(), totalMonths, "date")
		);
		setDateCaption(formatDouble(defaultSliderVal));
		return date;
	}
	

}

