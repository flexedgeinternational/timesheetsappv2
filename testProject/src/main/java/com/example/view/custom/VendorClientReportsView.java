package com.example.view.custom;

import com.example.util.GetResource;
import com.example.vaadin.core.view.AbstractGraphView;
import com.example.view.custom.graph.ContractLengthView;
import com.vaadin.addon.charts.Chart;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.themes.BaseTheme;

@SuppressWarnings("serial")
@SpringView(name = "VendorClientReportsView")
public class VendorClientReportsView extends AbstractGraphView<Object> implements View {

	String contractId;
	
	@Override
	public void enter(ViewChangeEvent event) {
		contractId = event.getParameters();
	}
	
	@Override
	protected Button[] options() {
		
		Button pictureButton = new NativeButton("Chart");
        pictureButton.setStyleName(BaseTheme.BUTTON_LINK);
        pictureButton.setIcon(GetResource.barGraph_small("Chart"));

        pictureButton.addClickListener(e -> {
			buildChart(new ContractLengthView(1, 12, 100000));
        });
		
		return new Button[] { 
			pictureButton
		};
	}

	@Override
	protected String getPreviousView() {
		return "VendorReportView";
	}
	
	@Override
	protected String getViewTitle() {
		return "Charts";
	}

	
	@Override
	protected Class<Object> getEntityClass() {
		return Object.class;
	}
	
	@Override
	protected Component getChart() {
		
		if (chart == null) return new Chart();
		
		return chart;
	}


}
