package com.example.view.custom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.example.localization.Lang;
import com.example.localization.Localizer;
import com.example.service.crud.ResourceService;
import com.example.view.AppUserListView;
import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;

@SuppressWarnings("serial")
@SpringView(name = "ResourceListView")
public class ResourceListView extends AppUserListView implements View {

	@Autowired
	@Qualifier("resourceService")
	ResourceService resourceService;
	
	@Override
	protected void initService() {
		this.service = resourceService;
	}

	@Override
	protected String getViewTitle() {
		return Lang.t("resources"); 
	}
	
	@Override
	protected void initFooterContent() {
		Button cancelButton = new Button(Lang.t("cancel"));
		cancelButton.setIcon(FontAwesome.BACKWARD);
		cancelButton.addClickListener(e -> {
			navigator.navigateTo("VendorHomeView");
		});
		
		footerLayout.addComponent(cancelButton);
	}
	
	protected void initCreateButton() {
		createButton = new Button(Localizer.getLocalizedTerm("New"), FontAwesome.PLUS);
		actionsLayout.addComponent(createButton);
		actionsLayout.setExpandRatio(createButton, 1);
		createButton.addClickListener(e -> {
			navigator.navigateTo("ResourceAddView");
		});
	}
	
	protected String getEditViewName() {
		return "ResourceEditView";
	}

	@Override
	protected boolean addEditInWindow() {
		return false;
	}
	
}
