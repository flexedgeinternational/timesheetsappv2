package com.example.view.custom;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.localization.Lang;
import com.example.model.Company;
import com.example.model.File;
import com.example.model.Frequency;
import com.example.model.Person;
import com.example.model.TimesheetTemplate;
import com.example.service.crud.EntityService;
import com.example.service.crud.FileService;
import com.example.service.crud.FrequencyService;
import com.example.service.crud.TimesheetTemplateService;
import com.example.util.GetResource;
import com.example.vaadin.component.CustomSelect;
import com.example.vaadin.component.DropBox;
import com.example.vaadin.core.window.GenericEditWindow;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.DragAndDropWrapper;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@SpringView(name = "TimesheetTemplateEditView")
public class TimesheetTemplateEditView extends com.example.vaadin.core.view.AbstractEditView<TimesheetTemplate> {

	@Autowired
	TimesheetTemplateService timesheetTemplateService;
	
	@Autowired
	FileService fileService;
	
	@Autowired
	FrequencyService frequencyService;
	
	DropBox dropoffBox;
	CustomSelect frequency;
	
	@Override
	protected void initService() {
		this.service = timesheetTemplateService;
	}

	@Override
	protected void initFormFields() {
		TextField name = new TextField("Template Name");
		name.setNullRepresentation("");

		frequency = new CustomSelect("Frequency");
		frequency.populate_objArrList(frequencyService.getFrequencies());
		
		dropoffBox = documentDropOff();
		
		formLayout.setMargin(true);
		formLayout.setSpacing(true);
		formLayout.addComponents(
			name, 
			frequency,
			dropoffBox
		);

		binder.bind(name, "name");

	}
	
	protected DropBox documentDropOff() {
		final Label infoLabel = new Label("Drag and Drop new template here");
		infoLabel.setStyleName("draganddropInfoLabel"); 
		
        final VerticalLayout dropPane = new VerticalLayout(infoLabel);
        dropPane.setComponentAlignment(infoLabel, Alignment.MIDDLE_CENTER);
        dropPane.addStyleName("drop-area");
 
        final DropBox dropBox = new DropBox(dropPane);
        dropBox.setSizeUndefined();
 
        ProgressBar progress = new ProgressBar();
        progress.setIndeterminate(true);
        progress.setVisible(false);
        dropPane.addComponent(progress);
        
        return dropBox; 
	}
	
	@Override
	protected void saveListner(
		EntityService<TimesheetTemplate> service, 
		GenericEditWindow window, 
		BeanFieldGroup<TimesheetTemplate> binder,
		TimesheetTemplate currentObject, 
		String getViewTitle, 
		boolean editWindow) {
			Map<String, ByteArrayOutputStream> files = dropoffBox.getFiles(); 
			
			ByteArrayOutputStream file = null;
			String fileName = "-1";
			
			if (files.size() > 1) {
				Notification.show(Lang.t("one.file.upload"), Type.WARNING_MESSAGE);
				return;
			}
			
			Iterator it = files.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        
		        fileName = pair.getKey().toString();
		        file = (ByteArrayOutputStream)pair.getValue();

		        // only iterate once 
		        break;
		    }
			
		    File fileObj = new File();
		    fileObj.setFile(file.toByteArray());
		    fileService.saveObject(fileObj);
		   
		    currentObject.setFile_name(fileName);
		    currentObject.setFile(fileObj);
		    
		    Frequency frequencyObj = frequencyService.loadObject(Long.valueOf(frequency.getValue().toString()));
		    currentObject.setFrequency(frequencyObj);
		    
		    super.saveListner(service, null, binder, currentObject, getViewTitle, false);

			
	}

	@Override
	protected Class<TimesheetTemplate> getEntityClass() {
		return TimesheetTemplate.class;
	}

}
