package com.example.view.custom;

import java.io.ByteArrayInputStream;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.localization.Lang;
import com.example.localization.Localizer;
import java.io.IOException;

import com.example.model.Role;
import com.example.model.TimesheetTemplate;
import com.example.service.crud.FileService;
import com.example.service.crud.TimesheetTemplateService;
import com.example.vaadin.core.view.AbstractListView;
import com.example.vaadin.core.window.GenericEditWindow;
import com.vaadin.addon.spreadsheet.Spreadsheet;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
@SpringView(name = "TimesheetTemplateListView")
public class TimesheetTemplateListView extends AbstractListView<TimesheetTemplate> implements View {

	@Autowired
	TimesheetTemplateService timesheetTemplateService;
	
	@Autowired
	FileService fileService;
	
	protected Button previewButton;
	protected Button setDefault;
	
	@Override
	public void enter(ViewChangeEvent event) { 
		
	}

	@Override
	protected void initContent() {
		super.initContent();
		previewButton = new Button(Localizer.getLocalizedTerm("preview"), FontAwesome.FILE_EXCEL_O);
		actionsLayout.addComponent(previewButton);
		actionsLayout.setExpandRatio(previewButton, 1);
		previewButton.setEnabled(false);
		previewButton.addClickListener(e -> {
			
			Collection<Object> selectedRows = listGrid.getSelectedRows();
			if (selectedRows.size() == 1) {
				TimesheetTemplate timesheetTemplate = (TimesheetTemplate) selectedRows.iterator().next();
				
				com.example.model.File myFile = timesheetTemplate.getFile();
				VerticalLayout vLayout = new VerticalLayout();
				ByteArrayInputStream bis = new ByteArrayInputStream(myFile.getFile());
				
				Spreadsheet template;
				try {
					template = new Spreadsheet(bis);
					template.setReadOnly(true);
					template.setSizeFull();
					
					vLayout.addComponent(template);
					
				} catch (IOException e1) {
					Notification.show(Lang.t("error.template"), Notification.Type.ERROR_MESSAGE);
					e1.printStackTrace();
				}
				
				//GenericEditWindow templatePopup = new GenericEditWindow(null, null, null);
				//templatePopup.initContent();
				// create popup window 
				Window templatePopup = new Window();
				templatePopup.setWidth("750px");
				templatePopup.setHeight("500px");
				templatePopup.setCaption(timesheetTemplate.getFile_name());
				templatePopup.setDraggable(true);
				templatePopup.setResizeLazy(true);
				templatePopup.setResizable(true);
				templatePopup.setClosable(true);
				templatePopup.setContent(vLayout);
				templatePopup.setPosition(200, 100);
				UI.getCurrent().addWindow(templatePopup);
				
			}
		});
		
		setDefault = new Button(Lang.t("set.default"));
		actionsLayout.addComponent(setDefault);
		actionsLayout.setExpandRatio(setDefault, 1);
		setDefault.setEnabled(false);
		setDefault.addClickListener(e -> {
			Collection<Object> selectedRows = listGrid.getSelectedRows();
			if (selectedRows.size() == 1) {
				TimesheetTemplate timesheetTemplate = (TimesheetTemplate) selectedRows.iterator().next();
				timesheetTemplateService.setAsDefault(timesheetTemplate.getOid());
				Notification.show(timesheetTemplate.getName() + " " + Lang.t("set.default"));
				reloadGridData();
			} else {
				Notification.show(Lang.t("only.one.row.may.be.selected"));
			}
		});
		
	}
	
	// Override the default functions action listener to unhide preview button
	@Override
	protected void initGrid() {
		super.initGrid();
		listGrid.addSelectionListener(e -> {
			Integer selectedRowSize = listGrid.getSelectedRows().size();
			deleteButton.setEnabled(selectedRowSize > 0);
			editButton.setEnabled(selectedRowSize == 1);
			previewButton.setEnabled(selectedRowSize == 1);
			setDefault.setEnabled(selectedRowSize == 1);
		});
	}
	
	@Override
	protected Object[] visibleColumns() {
		return new Object[] { "name", "file_name", "_default" };
	}

	@Override
	protected void initService() {
		this.service = timesheetTemplateService;
	}

	@Override
	protected void initFooterContent() {
		Button cancelButton = new Button(Lang.t("back"));
		cancelButton.setIcon(FontAwesome.BACKWARD);
		cancelButton.addClickListener(e -> {
			navigator.navigateTo("VendorParameterView");
		});
		
		footerLayout.addComponent(cancelButton);
	}

	@Override
	protected boolean addEditInWindow() {
		return false;
	}
	
	@Override
	protected String getViewTitle() {
		return Lang.t("timesheet.template");
	}

	@Override
	protected Class<TimesheetTemplate> getEntityClass() {
		return TimesheetTemplate.class;
	}

}
