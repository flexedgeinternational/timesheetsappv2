package com.example.view.custom;

import com.example.localization.Localizer;
import com.example.model.AppUser;
import com.example.view.AppUserEditView;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
@SpringView(name = "ResourceEditView")
public class ResourceEditView extends AppUserEditView implements View {

	@Override
	protected void initFormFields() {
		firstname = new TextField(Localizer.getLocalizedTerm("firstname"));
		firstname.setNullRepresentation("");

		lastname = new TextField(Localizer.getLocalizedTerm("lastname"));
		lastname.setNullRepresentation("");

		username = new TextField(Localizer.getLocalizedTerm("username"));
		username.setNullRepresentation("");

		email = new TextField(Localizer.getLocalizedTerm("email"));
		email.setNullRepresentation("");
		
		active = new CheckBox(Localizer.getLocalizedTerm("active"));
		
		formLayout.addComponents(username, firstname, lastname, email, active);
		
		binder.bind(firstname, "firstname");
		binder.bind(lastname, "lastname");
		binder.bind(username, "username");
		binder.bind(email, "email");
		binder.bind(active, "active");
		
		/* not entering passwords so don't check if they're equal */
		valide = true;
	}
	
	@Override
	protected void updateEditActionVisibility(AppUser currentObject, String id) {

	}

	@Override
	protected void initFooterContent() {
		super.initFooterContent();
		cancelButton.addClickListener(e -> {
			navigator.navigateTo("ResourceListView");
		});
	}
	
}
