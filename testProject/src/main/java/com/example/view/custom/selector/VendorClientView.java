package com.example.view.custom.selector;

import java.util.ArrayList;

import com.example.localization.Lang;
import com.example.vaadin.core.view.AbstractCircleSelectorView;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;

@SuppressWarnings("serial")
@SpringView(name = "VendorClientView")
public class VendorClientView extends AbstractCircleSelectorView<Object> implements View {

	@Override
	protected ArrayList<Component> options() {

		list.add(buildNavButton(
			"Home", "VendorHomeView"
		));
		list.add(buildNavButton(
			"Client Contacts", "PersonListView"
		));
		list.add(buildNavButton(
			"Client Organizations", "CompanyListView"
		));
		
		return list;
		
	}

	@Override
	protected String getListViewName() {
		return Lang.t("client.info");
	}

	
	
}
