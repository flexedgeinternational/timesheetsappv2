 
package com.example.view.custom;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.example.localization.Lang;
import com.example.localization.Localizer;
import com.example.model.Contract;
import com.example.service.crud.CompanyService;
import com.example.service.crud.ContractService;
import com.example.service.crud.CurrencyService;
import com.example.service.crud.PersonService;
import com.example.vaadin.component.CustomSelect;
import com.example.vaadin.core.view.AbstractListView;
import com.example.vaadin.core.window.GenericEditWindow;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
@SpringView(name = "ContractListView")
public class ContractListView extends AbstractListView<Contract> implements View {

	@Autowired
	ContractService contractService;

	@Autowired
	CurrencyService currencyService;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	PersonService personService;
	
	protected Button closeContractButton;
	
	@Override
	public void enter(ViewChangeEvent event) {

	}
	
	@Override
	protected void initContent() {
		super.initContent();
		closeContractButton = new Button(Localizer.getLocalizedTerm("close"), FontAwesome.LOCK);
		actionsLayout.addComponent(closeContractButton);
		actionsLayout.setExpandRatio(closeContractButton, 1);
		closeContractButton.setEnabled(false);
		
		closeContractButton.addClickListener(e -> {
			Integer selectedRowSize = listGrid.getSelectedRows().size();
			if (selectedRowSize != 1) {
				Notification.show(Lang.t("only.one.row.may.be.selected"));
				return;
			}
			
			contractService.closeContract(contractService.getSelectedRowId(listGrid.getSelectedRows().iterator().next()));
			reloadGridData();
			Notification.show(Lang.t("status.closed"));
			
		});
		
	}
	
	// Override the default functions action listener to unhide preview button
	@Override
	protected void initGrid() {
		super.initGrid();
		listGrid.addSelectionListener(e -> {
			Integer selectedRowSize = listGrid.getSelectedRows().size();
			deleteButton.setEnabled(selectedRowSize > 0);
			editButton.setEnabled(selectedRowSize == 1);
			closeContractButton.setEnabled(selectedRowSize == 1);
		});
	}
	
	@Override
	protected Object[] visibleColumns() {
		return new Object[] { "contractTitle", "client", "status" };
	}
	
	@Override
	protected void initWindowEditFields(GenericEditWindow window) {
		TextField firstname = new TextField("Contract Title");
		firstname.setNullRepresentation("");
		window.addField(firstname, "contractTitle");

		TextField lastname = new TextField("Contract Identifier");
		lastname.setNullRepresentation("");
		window.addField(lastname, "contractIdentifier");

		TextField totalCost = new TextField("Total Cost");
		totalCost.setNullRepresentation("");
		window.addField(totalCost, "totalCost");
		
		CustomSelect currency = new CustomSelect("Currency");
		currency.populate_strList(currencyService.getCurrencies());
		window.addField(currency, "currency");
		
		// Generate list of companies 
		List<Object[]> companies = companyService.getCompanies();
		
		CustomSelect client = new CustomSelect("Client");
		client.populate_objArrList(companies);
		client.setNewItemsAllowed(false);
		window.addField(client, "client");
		
		CustomSelect serviceProvider = new CustomSelect("Service Provider");
		serviceProvider.populate_objArrList(companies);
		serviceProvider.setNewItemsAllowed(false);
		window.addField(serviceProvider, "serviceProvider");

		// Generate list of people
		List<Object[]> people = personService.getPeople();
		
		CustomSelect contractingAuth = new CustomSelect("Contracting Authority");
		contractingAuth.populate_objArrList(people);
		contractingAuth.setNewItemsAllowed(false);
		window.addField(contractingAuth, "contractingAuth");
		
		CustomSelect technicalAuth = new CustomSelect("Technical Authority");
		technicalAuth.populate_objArrList(people);
		technicalAuth.setNewItemsAllowed(false);
		window.addField(technicalAuth, "technicalAuth");
		
		CustomSelect vendorAuth = new CustomSelect("Vendor Authority");
		vendorAuth.populate_objArrList(people);
		vendorAuth.setNewItemsAllowed(false);
		window.addField(vendorAuth, "vendorAuth");
		
		DateField startDate = new DateField("Start Date");
		window.addField(startDate, "startDate");
		
		DateField endDate = new DateField("End Date");
		window.addField(endDate, "endDate");
		
		TextField status = new TextField("Status");
		status.setNullRepresentation("");
		window.addField(status, "status");
	}

	@Override
	protected boolean addEditInWindow() {
		return true;
	}

	@Override
	protected void initFooterContent() {	
		Button cancelButton = new Button(Lang.t("cancel"));
		cancelButton.setIcon(FontAwesome.BACKWARD);
		cancelButton.addClickListener(e -> {
			navigator.navigateTo("VendorHomeView");
		});
		
		footerLayout.addComponent(cancelButton);
	}
	
	@Override
	protected void initService() {
		this.service = contractService;
	}

	@Override
	protected String getViewTitle() {
		return Lang.t("mycontracts");
	}

	protected String getEditViewName() {
		return service.getEntityClass().getSimpleName() + "EditView";
	}
	
	@Override
	protected Class<Contract> getEntityClass() {
		return Contract.class;
	}
}
