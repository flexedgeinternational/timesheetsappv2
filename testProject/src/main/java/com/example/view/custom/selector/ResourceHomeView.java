package com.example.view.custom.selector;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.example.localization.Lang;
import com.example.model.AppUser;
import com.example.service.crud.ContractService;
import com.example.vaadin.core.view.AbstractCircleSelectorView;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@SpringView(name = "ResourceHomeView")
public class ResourceHomeView extends AbstractCircleSelectorView<Object> implements View {

	@Autowired
	ContractService contractService;
	
	AppUser appUser = (AppUser)UI.getCurrent().getSession().getAttribute(AppUser.class.getName());
	
	@Override
	protected ArrayList<Component> options() {

		Button homeBtn = new Button("Home");
		homeBtn.setEnabled(false);
		list.add(homeBtn);
		
		List<Object[]> contracts = contractService.getResourceContracts(appUser.getUsername());
		
		for (Object[] arr : contracts) {
			list.add(buildNavButton(
				arr[2] + " / " + arr[1], "ResourceTimesheetListView" + "/" + arr[0].toString()
			));
		}
		
		list.add(buildNavButton(
			"Parameters", "ResourceParameterView"
		));
		list.add(buildNavButton(
			"Action Items", ""
		));
		list.add(buildNavButton(
			"Reports", "ResourceReportsView"
		));
		list.add(buildNavButton(
			"Closed Contracts", "ResourceClosedContractListView"
		));
		
		return list;
	}

	@Override
	protected String getListViewName() {
		return Lang.t("resource");
	} 

}
