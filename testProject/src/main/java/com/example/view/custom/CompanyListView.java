package com.example.view.custom;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.localization.Lang;
import com.example.model.Company;
import com.example.service.crud.CompanyService;
import com.example.vaadin.core.view.AbstractListView;
import com.example.vaadin.core.window.GenericEditWindow;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
@SpringView(name = "CompanyListView")
public class CompanyListView extends AbstractListView<Company> implements View {

	@Autowired
	CompanyService companyService;

	
	@Override
	public void enter(ViewChangeEvent event) {
	}

	@Override
	protected Object[] visibleColumns() {
		return new Object[] { "acronym", "name" };
	}

	@Override
	protected void initWindowEditFields(GenericEditWindow window) {
		TextField name = new TextField("Name");
		name.setNullRepresentation("");
		window.addField(name, "name");

		TextField acronym = new TextField("Acronym");
		acronym.setNullRepresentation("");
		window.addField(acronym, "acronym");

		TextField street_addr = new TextField("Street Address");
		street_addr.setNullRepresentation("");
		window.addField(street_addr, "street_addr");
		
		TextField email = new TextField("email");
		email.setNullRepresentation("");
		window.addField(email, "email");
		
		TextField floor = new TextField("floor");
		floor.setNullRepresentation("");
		window.addField(floor, "floor");
		
		TextField city = new TextField("city");
		city.setNullRepresentation("");
		window.addField(city, "city");
		
		TextField province = new TextField("Province");
		province.setNullRepresentation("");
		window.addField(province, "province");
		
		TextField postal_code = new TextField("Postal Code");
		postal_code.setNullRepresentation("");
		window.addField(postal_code, "postal_code");

	}

	@Override
	protected boolean addEditInWindow() {
		return true;
	}
	
	
	@Override
	protected void initService() {
		this.service = companyService;
	}

	@Override
	protected void initFooterContent() {
		Button cancelButton = new Button(Lang.t("back"));
		cancelButton.setIcon(FontAwesome.BACKWARD);
		cancelButton.addClickListener(e -> {
			navigator.navigateTo("VendorClientView");
		});
		
		footerLayout.addComponent(cancelButton);
	}

	@Override
	protected String getViewTitle() {
		return Lang.t("client.organizations");
	}

	@Override
	protected Class<Company> getEntityClass() {
		return Company.class;
	}

}
