package com.example.view.custom;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.model.Company;
import com.example.model.Person;
import com.example.service.crud.CompanyService;
import com.example.service.crud.EntityService;
import com.example.service.crud.PersonService;
import com.example.vaadin.component.CustomSelect;
import com.example.vaadin.core.window.GenericEditWindow;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
@SpringView(name = "PersonEditView")
public class PersonEditView extends com.example.vaadin.core.view.AbstractEditView<Person> {


	@Autowired
	PersonService personService;
	
	@Autowired
	CompanyService companyService;
	
	private CustomSelect companyCustomSelect;
	
	@Override
	protected void initService() {
		this.service = personService;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		super.enter(event);
		
		if (event.getParameters() != "" || !objectId.isEmpty())  
			companyCustomSelect.select(String.valueOf(currentObject.getCompany().getOid()));
	}
	
	@Override
	protected void initFormFields() {
		TextField first_name = new TextField("First Name");
		first_name.setNullRepresentation("");

		TextField last_name = new TextField("Last Name");
		last_name.setNullRepresentation("");

		TextField email = new TextField("Email");
		email.setNullRepresentation("");
		
		TextField province = new TextField("Province");
		province.setNullRepresentation("");
		
		TextField fax_number = new TextField("Fax Number");
		fax_number.setNullRepresentation("");
		
		TextField work_number = new TextField("Work Number");
		work_number.setNullRepresentation("");
		
		TextField cell_number = new TextField("Cell Number");
		cell_number.setNullRepresentation("");
		
		companyCustomSelect = new CustomSelect("Company");
		companyCustomSelect.populate_objArrList(companyService.getCompanies());
		
		formLayout.setMargin(true);
		formLayout.setSpacing(true);
		formLayout.addComponents(
			first_name, 
			last_name, 
			email, 
			province, 
			fax_number, 
			work_number, 
			cell_number, 
			companyCustomSelect
		);

		binder.bind(first_name, "first_name");
		binder.bind(last_name, "last_name");
		binder.bind(email, "email");
		binder.bind(province, "province");
		binder.bind(fax_number, "fax_number");
		binder.bind(work_number, "work_number");
		binder.bind(cell_number, "cell_number");
		
	}

	@Override
	protected void saveListner(
		EntityService<Person> service, 
		GenericEditWindow window, 
		BeanFieldGroup<Person> binder,
		Person currentObject, 
		String getViewTitle, 
		boolean editWindow) {
			Company company = companyService.loadObject(Long.valueOf(companyCustomSelect.getValue().toString()));
			currentObject.setCompany(company);
			super.saveListner(service, null, binder, currentObject, getViewTitle, false);
	}

	@Override
	protected Class<Person> getEntityClass() {
		return Person.class;
	}

}

