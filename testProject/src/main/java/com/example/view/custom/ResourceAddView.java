package com.example.view.custom;

import com.example.model.Role;
import com.example.view.AppUserEditView;
import com.vaadin.data.Container;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;

@SuppressWarnings("serial")
@SpringView(name = "ResourceAddView")
public class ResourceAddView extends AppUserEditView implements View {
	
	@Override
	protected void initFormFields() {
		super.initFormFields();
		
		Container container = roleGrid.getContainerDataSource();
		
		for( Object role : container.getItemIds()) {
			Role lRole = (Role)role;
			if (lRole.getName().equals("Resource")) {
				roleGrid.select(lRole);
			}
		}
		
		roleGrid.setVisible(false);
		
	}
}
