package com.example.view.custom;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.example.localization.Lang;
import com.example.localization.Localizer;
import com.example.model.AppUser;
import com.example.model.Timesheet;
import com.example.service.crud.ResourceTimesheetService;
import com.example.service.crud.TimesheetTemplateService;
import com.example.vaadin.core.view.AbstractListView;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SelectionMode;

@SuppressWarnings("serial")
@SpringView(name = "ResourceTimesheetListView")
public class ResourceTimesheetListView extends AbstractListView<Timesheet> implements View   {

	@Autowired
	@Qualifier("resourceTimesheetService")
	ResourceTimesheetService resourceTimesheetService;
	
	@Autowired
	TimesheetTemplateService timesheetTemplateService;
	
	AppUser appUser = (AppUser)UI.getCurrent().getSession().getAttribute(AppUser.class.getName());
	
	@Override
	public void enter(ViewChangeEvent event) {
		objectId = event.getParameters();
		reloadGridData();
	}

	@Override
	protected void initGrid() {
		super.initGrid();
		listGrid.getColumn("start_date").setHeaderCaption("Start Date");
		listGrid.getColumn("end_date").setHeaderCaption("End Date");
		listGrid.getColumn("reg_sum").setHeaderCaption("Total Regular Hours");
		listGrid.getColumn("ot_sum").setHeaderCaption("Total OT Hours");
	}
	
	// Find better way to do this 1-7-2017
	@Override
	protected void reloadGridData() {
		
		// skip on initial call before objectId is defined
		if (objectId == null || objectId.equals(null) || objectId.isEmpty()) {
			System.out.println("objectId is null");
			return;
		}
		
		// if here then called again by enter so objectId is now defined
		
		BeanItemContainer<Timesheet> container = 
			new BeanItemContainer<>(resourceTimesheetService.getEntityClass(), resourceTimesheetService.getListObjects(Integer.parseInt(objectId), appUser.getUsername()));
		
		listGrid.setContainerDataSource(container);
		// Only If Filter is a textField
		if (addFilterRow()) {
			i = i + 1;
			HeaderRow filterRow = null;
			if (i == 1) {
				filterRow = listGrid.appendHeaderRow();

				// Set up a filter for all columns
				for (Object pid : listGrid.getContainerDataSource().getContainerPropertyIds()) {
					HeaderCell cell = null;
					if (filterRow != null) {
						cell = filterRow.getCell(pid);
					}
					// Have an input field to use for filter
					TextField filterField = new TextField();
					// filterField.setColumns(8);

					// Update filter When the filter input is changed
					filterField.addTextChangeListener(change -> {
						// Can't modify filters so need to replace
						container.removeContainerFilters(pid);

						// (Re)create the filter if necessary
						if (!change.getText().isEmpty())
							container.addContainerFilter(new SimpleStringFilter(pid, change.getText(), true, false));
					});
					listOfFieldFilter.put(pid, filterField);
					if ((filterField != null) && (cell != null)) {
						cell.setComponent(filterField);
					}

				}
			}
			for (Object pid : listOfFieldFilter.keySet()) {
				if (listOfFieldFilter.get(pid) != null) {
					((TextField) listOfFieldFilter.get(pid)).addTextChangeListener(change -> {
						// Can't modify filters so need to replace
						container.removeContainerFilters(pid);
						// (Re)create the filter if necessary
						if (!change.getText().isEmpty())
							container.addContainerFilter(new SimpleStringFilter(pid, change.getText(), true, false));
					});

				}
			}
		}
	}
	
	@Override
	protected void initCreateButton() {
		super.initCreateButton();
		
		createButton.addClickListener(e -> {
			navigator.navigateTo(getEditViewName() + "/" + objectId);
		});
	}
	
	@Override
	protected void initEditButton() {
		super.initEditButton();
		editButton.addClickListener(e -> {

			Collection<Object> selectedRows = listGrid.getSelectedRows();
			if (selectedRows.size() == 1) {
				Timesheet row = (Timesheet) selectedRows.iterator().next();
				navigator.navigateTo(getEditViewName() + "/" + objectId + "/" + service.getSelectedRowId(row));
			} else {
				Notification.show(Lang.t("only.one.row.may.be.selected"));
			}
		});
	}
	
	@Override
	protected boolean addEditInWindow() {
		return false;
	}

	@Override
	protected String getListViewName() {
		return "Resource" + resourceTimesheetService.getEntityClass().getSimpleName() + "ListView";
	}
	
	@Override
	protected String getEditViewName() {
		return "Resource" + resourceTimesheetService.getEntityClass().getSimpleName() + "EditView";
	}
	
	@Override
	protected Object[] visibleColumns() {
		return new Object[] { "start_date", "end_date", "reg_sum", "ot_sum" };
	}

	@Override
	protected void initService() {
		this.service = resourceTimesheetService;
	}

	@Override
	protected void initFooterContent() {
		Button backButton = new Button(Lang.t("back"));
		backButton.setIcon(FontAwesome.BACKWARD);
		backButton.addClickListener(e -> {
			navigator.navigateTo("ResourceHomeView");
		});
		
		footerLayout.addComponent(backButton);
	}

	@Override
	protected String getViewTitle() {
		return "Contract title";
		//return Lang.t("timesheet.template");
	}

	@Override
	protected Class<Timesheet> getEntityClass() {
		return Timesheet.class;
	}

}
