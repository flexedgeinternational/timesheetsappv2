package com.example.view.custom.selector;

import java.util.ArrayList;

import com.example.localization.Lang;
import com.example.vaadin.core.view.AbstractCircleSelectorView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;

@SuppressWarnings("serial")
@SpringView(name = "VendorHomeView")
public class VendorHomeView extends AbstractCircleSelectorView<Object> implements View {

	@Override
	protected ArrayList<Component> options() {
		
		Button homeBtn = new Button("Home");
		homeBtn.setEnabled(false);
		list.add(homeBtn);
		
		list.add(buildNavButton(
			"Clients", "VendorClientView"
		));
		list.add(buildNavButton(
			"Resources", "ResourceListView"
		));
		list.add(buildNavButton(
			"Contracts", "ContractListView"
		));
		list.add(buildNavButton(
			"Reports", "VendorReportView"
		));
		list.add(buildNavButton(
			"Action Items", ""
		));
		list.add(buildNavButton(
			"Parameters", "VendorParameterView"
		));
		
		
		return list;
	}

	@Override
	protected String getListViewName() {
		return Lang.t("vendor");
	} 

}
