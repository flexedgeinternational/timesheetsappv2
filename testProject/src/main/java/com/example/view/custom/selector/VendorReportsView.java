package com.example.view.custom.selector;

import java.util.ArrayList;

import com.example.localization.Lang;
import com.example.model.AppUser;
import com.example.vaadin.core.view.AbstractCircleSelectorView;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@SpringView(name = "VendorReportsView")
public class VendorReportsView extends AbstractCircleSelectorView<Object> implements View {

	AppUser appUser = (AppUser)UI.getCurrent().getSession().getAttribute(AppUser.class.getName());
	
	@Override
	protected ArrayList<Component> options() {

		list.add(buildNavButton(
			"Home", "VendorHomeView"
		));
		
		list.add(buildNavButton(
			"Client Reports", "VendorClientReportsView"
		));

		list.add(buildNavButton(
			"Resource Reports", "VendorResourceReportsView"
		));
		
		return list;
	}

	@Override
	protected String getListViewName() {
		return Lang.t("resource");
	} 

}
