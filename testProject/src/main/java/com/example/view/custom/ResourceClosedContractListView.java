package com.example.view.custom;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.localization.Lang;
import com.example.model.AppUser;
import com.example.model.Contract;
import com.example.service.crud.ContractService;
import com.example.vaadin.core.view.AbstractListView;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SelectionMode;

@SuppressWarnings("serial")
@SpringView(name = "ResourceClosedContractListView")
public class ResourceClosedContractListView extends AbstractListView<Contract> implements View  {

	@Autowired
	ContractService contractService;

	AppUser appUser = (AppUser)UI.getCurrent().getSession().getAttribute(AppUser.class.getName());
	
	protected Button viewButton;
	
	@Override
	public void enter(ViewChangeEvent event) {
		
	}
	
	protected void initViewButton() {
		viewButton = new Button(Lang.t("view.timesheets"));
		actionsLayout.addComponent(viewButton);
		actionsLayout.setExpandRatio(viewButton, 1);
		viewButton.setEnabled(false);
		viewButton.addClickListener(e -> {
			Collection<Object> selectedRows = listGrid.getSelectedRows();
			if (selectedRows.size() == 1) {
				Contract row = (Contract) selectedRows.iterator().next();
				navigator.navigateTo("ResourceTimesheetClosedContractListView" 
						+ "/" + service.getSelectedRowId(row));
			}
		});
	}
	
	@Override
	protected void initGrid() {
		
		initViewButton();
		
		listGrid = new Grid();
		// Activate multi selection mode
		listGrid.setSelectionMode(SelectionMode.MULTI);
		listGrid.setSizeFull();
		reloadGridData();
		listGrid.setColumns(visibleColumns());
		listGrid.addSelectionListener(e -> {
			Integer selectedRowSize = listGrid.getSelectedRows().size();
			viewButton.setEnabled(selectedRowSize == 1);
		});
		if (selectionItemGrid()) {
			listGrid.addItemClickListener(new ItemClickListener() {
				@Override
				public void itemClick(ItemClickEvent event) {
					itemGridClickListner((Contract) event.getItemId());
				}
			});
		}
		contentLayout.addComponent(listGrid);
		contentLayout.setExpandRatio(listGrid, 1);

	}
	
	// TO-DO Find better way to do this 1-7-2017
	@Override
	protected void reloadGridData() {
		
		// if here then called again by enter so objectId is now defined
		BeanItemContainer<Contract> container = 
			new BeanItemContainer<>(contractService.getEntityClass(), contractService.getListObjects(appUser));
		
		listGrid.setContainerDataSource(container);
		// Only If Filter is a textField
		if (addFilterRow()) {
			i = i + 1;
			HeaderRow filterRow = null;
			if (i == 1) {
				filterRow = listGrid.appendHeaderRow();

				// Set up a filter for all columns
				for (Object pid : listGrid.getContainerDataSource().getContainerPropertyIds()) {
					HeaderCell cell = null;
					if (filterRow != null) {
						cell = filterRow.getCell(pid);
					}
					// Have an input field to use for filter
					TextField filterField = new TextField();
					// filterField.setColumns(8);

					// Update filter When the filter input is changed
					filterField.addTextChangeListener(change -> {
						// Can't modify filters so need to replace
						container.removeContainerFilters(pid);

						// (Re)create the filter if necessary
						if (!change.getText().isEmpty())
							container.addContainerFilter(new SimpleStringFilter(pid, change.getText(), true, false));
					});
					listOfFieldFilter.put(pid, filterField);
					if ((filterField != null) && (cell != null)) {
						cell.setComponent(filterField);
					}

				}
			}
			for (Object pid : listOfFieldFilter.keySet()) {
				if (listOfFieldFilter.get(pid) != null) {
					((TextField) listOfFieldFilter.get(pid)).addTextChangeListener(change -> {
						// Can't modify filters so need to replace
						container.removeContainerFilters(pid);
						// (Re)create the filter if necessary
						if (!change.getText().isEmpty())
							container.addContainerFilter(new SimpleStringFilter(pid, change.getText(), true, false));
					});

				}
			}
		}
	}

	@Override
	protected Object[] visibleColumns() {
		return new Object[] { "contractTitle", "client", "status" };
	}

	@Override
	protected void initService() {
		this.service = contractService;
	}

	@Override
	public void initEditButton() { }
	
	@Override
	public void initDeleteButton() { }
	
	@Override
	public void initCreateButton() { }
	
	@Override
	protected void initFooterContent() {
		Button cancelButton = new Button(Lang.t("cancel"));
		cancelButton.setIcon(FontAwesome.BACKWARD);
		cancelButton.addClickListener(e -> {
			navigator.navigateTo("ResourceHomeView");
		});
		
		footerLayout.addComponent(cancelButton);
	}

	@Override
	protected String getViewTitle() {
		return Lang.t("closed.contracts");
	}

	@Override
	protected Class<Contract> getEntityClass() {
		return Contract.class;
	}
	
}
