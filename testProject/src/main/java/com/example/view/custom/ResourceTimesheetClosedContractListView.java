package com.example.view.custom;

import com.example.localization.Lang;
import com.example.model.Timesheet;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Grid.SelectionMode;

@SuppressWarnings("serial")
@SpringView(name = "ResourceTimesheetClosedContractListView")
public class ResourceTimesheetClosedContractListView extends ResourceTimesheetListView {

	protected Button viewButton;
	
	@Override
	public void initEditButton() { }
	
	@Override
	public void initDeleteButton() { }
	
	@Override
	public void initCreateButton() { }
	
	protected void initViewButton() {
		viewButton = new Button(Lang.t("view.timesheet"));
		actionsLayout.addComponent(viewButton);
		actionsLayout.setExpandRatio(viewButton, 1);
		viewButton.setEnabled(false);
		viewButton.addClickListener(e -> {
			System.out.println("View timesheet!");
			Notification.show("Display timesheet");
		});
	}
	
	@Override
	protected void initGrid() {
		listGrid = new Grid();
		// Activate multi selection mode
		listGrid.setSelectionMode(SelectionMode.MULTI);
		listGrid.setSizeFull();
		reloadGridData();
		listGrid.setColumns(visibleColumns());
		listGrid.addSelectionListener(e -> {
			Integer selectedRowSize = listGrid.getSelectedRows().size();
			viewButton.setEnabled(selectedRowSize == 1);
		});
		if (selectionItemGrid()) {
			listGrid.addItemClickListener(new ItemClickListener() {
				@Override
				public void itemClick(ItemClickEvent event) {
					itemGridClickListner((Timesheet) event.getItemId());
				}
			});
		}
		contentLayout.addComponent(listGrid);
		contentLayout.setExpandRatio(listGrid, 1);

	}
	
	@Override
	public void initContent() {
		super.initContent();
		
		initViewButton();
		
	}
	
}
