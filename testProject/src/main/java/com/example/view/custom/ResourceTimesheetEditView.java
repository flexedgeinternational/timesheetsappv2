package com.example.view.custom;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.example.localization.Lang;
import com.example.localization.Localizer;
import com.example.model.AppUser;
import com.example.model.ContractResource;
import com.example.model.Timesheet;
import com.example.model.TimesheetTemplate;
import com.example.service.crud.ContractResourceService;
import com.example.service.crud.EntityService;
import com.example.service.crud.ResourceTimesheetService;
import com.example.service.crud.TimesheetTemplateService;
import com.example.vaadin.core.window.GenericEditWindow;
import com.vaadin.addon.spreadsheet.Spreadsheet;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Notification.Type;

@SuppressWarnings("serial")
@SpringView(name = "ResourceTimesheetEditView")
public class ResourceTimesheetEditView extends com.example.vaadin.core.view.AbstractEditView<Timesheet>  {

	@Autowired
	@Qualifier("resourceTimesheetService")
	ResourceTimesheetService resourceTimesheetService;
	
	@Autowired
	TimesheetTemplateService timesheetTemplateService;
	
	@Autowired
	ContractResourceService contractResourceService;
	
	AppUser appUser = (AppUser)UI.getCurrent().getSession().getAttribute(AppUser.class.getName());
	
	Spreadsheet template = null;
	
	TimesheetTemplate timesheetTemplate;
	
	protected DateField end_date;
	
	protected DateField start_date;
	
	protected long contractId;
	
	@Override
	public void enter(ViewChangeEvent event) {
		binder.clear();
		this.loadObject(event.getParameters());
	}
	
	
	@Override
	protected void loadObject(String id) {
		/*
		if (id == null || id.isEmpty()) {
			System.out.println("id is null");
			return;
		}
		*/
		System.out.println("params = " + id);
		
		List<String> allMatches = new ArrayList<String>();
		Matcher matcher = Pattern.compile("(\\d+)").matcher(id);
		
		if (matcher.groupCount() == 0) {
			Notification.show(Lang.t("unable.load.data"), Type.ERROR_MESSAGE);
			return;
		}
		
		while (matcher.find()) {
			allMatches.add(matcher.group());
		}

		if (allMatches.size() >= 1) {
			contractId = Long.parseLong(allMatches.get(0));
		}
		
		if (allMatches.size() == 2) {
			objectId = allMatches.get(1); 
		}

		if (objectId == null || objectId.isEmpty()) {
			currentObject = service.createObject();
			
			System.out.println("create currentObject id = " + currentObject.getOid());
			
		} else {
			currentObject = service.loadObject(Long.parseLong(objectId));
			
			System.out.println("load currentObject id = " + currentObject.getOid());
			
			template.getCell("D4").setCellValue(currentObject.getMon_reg());
			template.getCell("F4").setCellValue(currentObject.getTues_reg());
			template.getCell("H4").setCellValue(currentObject.getWed_reg());
			template.getCell("J4").setCellValue(currentObject.getThurs_reg());
			template.getCell("L4").setCellValue(currentObject.getFri_reg());
			template.getCell("M4").setCellValue(currentObject.getSat_reg());
			template.getCell("P4").setCellValue(currentObject.getSun_reg());
			
			template.getCell("E4").setCellValue(currentObject.getMon_ot());
			template.getCell("G4").setCellValue(currentObject.getTues_ot());
			template.getCell("I4").setCellValue(currentObject.getWed_ot());
			template.getCell("K4").setCellValue(currentObject.getThurs_ot());
			template.getCell("M4").setCellValue(currentObject.getFri_ot());
			template.getCell("O4").setCellValue(currentObject.getSat_ot());
			template.getCell("Q4").setCellValue(currentObject.getSun_ot());
			
		}
		
		updateEditActionVisibility(currentObject, id);
		binder.setItemDataSource(currentObject);
		binder.bindMemberFields(formLayout);
		binder.setBuffered(true);

		end_date.setValue(new Date());
		
	}

	@Override
	protected void cancelListner(GenericEditWindow window, BeanFieldGroup<Timesheet> binder, boolean editWindow) {
		binder.discard();
		navigator.navigateTo(getListViewName() + "/" + contractId);
	}
	
	@Override
	protected void initService() {
		this.service = resourceTimesheetService;
	}

	@Override
	protected void initFormFields() {
		
		HorizontalLayout hLayout = new HorizontalLayout();
		hLayout.setSizeFull();
		hLayout.setSpacing(true);
		start_date = new DateField("Start Date");

		end_date = new DateField("End Date");
		
		System.out.println("new Date() = " + new Date());
		
		hLayout.addComponent(start_date);
		hLayout.addComponent(end_date);
		
		// load default spreadsheet
		timesheetTemplate = timesheetTemplateService.getDefaultTemplate();
		
		if (timesheetTemplate == null) {
			Notification.show(Lang.t("please.upload.template"));
			return;
		}
		
		com.example.model.File myFile = timesheetTemplate.getFile();
		VerticalLayout vLayout = new VerticalLayout();
		
		vLayout.setHeight("100%");
		vLayout.setWidth("100%");
		vLayout.setSizeFull();
		
		/* TO-DO give timesheet template a getSpreadsheet function to do this */
		ByteArrayInputStream bis = new ByteArrayInputStream(myFile.getFile());
		
		try {
			template = new Spreadsheet(bis);
			template.setReadOnly(false);
			template.setSizeFull();
			template.setSizeFull();
			
			vLayout.addComponent(template);
			
		} catch (IOException e1) {
			Notification.show(Lang.t("error.template"), Notification.Type.ERROR_MESSAGE);
			e1.printStackTrace();
		}
		
		formLayout.setMargin(true);
		formLayout.setSpacing(true);
		formLayout.addComponents(
			hLayout,
			vLayout
		);
		
		binder.bind(start_date, "start_date");
		binder.bind(end_date, "end_date");
	}

	@Override
	protected void saveListner(
		EntityService<Timesheet> service, 
		GenericEditWindow window, 
		BeanFieldGroup<Timesheet> binder,
		Timesheet currentObject, 
		String getViewTitle, 
		boolean editWindow) {

		if (currentObject == null) {
			Notification.show("currentObject is null");
			return;
		}
		
		currentObject.setMon_reg(getCell("D4"));
		currentObject.setTues_reg(getCell("F4"));
		currentObject.setWed_reg(getCell("H4"));
		currentObject.setThurs_reg(getCell("J4"));
		currentObject.setFri_reg(getCell("L4"));
		currentObject.setSat_reg(getCell("N4"));
		currentObject.setSun_reg(getCell("P4"));
		
		currentObject.setMon_ot(getCell("E4"));
		currentObject.setTues_ot(getCell("G4"));
		currentObject.setWed_ot(getCell("I4"));
		currentObject.setThurs_ot(getCell("K4"));
		currentObject.setFri_ot(getCell("M4"));
		currentObject.setSat_ot(getCell("O4"));
		currentObject.setSun_ot(getCell("Q4"));
		
		Date date = new Date();
        currentObject.setSubmitted(new Timestamp(date.getTime()));
		
        System.out.println("new Timestamp(date.getTime()) = " + new Timestamp(date.getTime()));
        
		ContractResource contractResource = 
			contractResourceService.getContractResource(contractId, appUser.getUsername());
		
		currentObject.setContract_resource_oid(contractResource);
		currentObject.setTimesheet_oid(timesheetTemplate);
		
		try {
			binder.commit();
			resourceTimesheetService.saveObject(currentObject);

			Notification.show(Localizer.getLocalizedTerm("Saved successfully!"));
			navigator.navigateTo(getListViewName() + "/" + contractId);
			
			reloadGridData();
		} catch (CommitException e) {
			Notification.show(Localizer.getLocalizedTerm(Lang.t("validation.errors")));
		}
		
	}

	private Double getCell(String cellCode) {
		
		Double cellVal;
		
		try {
			Cell cell = template.getCell(cellCode);
			
			if (cell == null) {
				cellVal = 0.0;
			} else {
				cellVal = template.getCell(cellCode).getNumericCellValue();
			}
		} catch (Exception ex) {
			Notification.show("Please ensure " + cellCode + " is a numeric value");
			return null;
		}
		
		return cellVal;
	} 
	
	@Override
	protected String getListViewName() {
		return "Resource" + resourceTimesheetService.getEntityClass().getSimpleName() + "ListView";
	}
	
	@Override
	protected Class<Timesheet> getEntityClass() {
		return Timesheet.class;
	}

}
