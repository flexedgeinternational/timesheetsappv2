package com.example.view.custom.selector;

import java.util.ArrayList;

import com.example.localization.Lang;
import com.example.vaadin.core.view.AbstractCircleSelectorView;
import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;

@SuppressWarnings("serial")
@SpringView(name = "ResourceParameterView")
public class ResourceParameterView extends AbstractCircleSelectorView<Object> implements View {

	@Override
	protected ArrayList<Component> options() {

		list.add(buildNavButton(
			"Home", "ResourceHomeView"
		));
		list.add(buildNavButton(
			"Salutations", ""
		));
		list.add(buildNavButton(
			"Timesheet Template", ""
		));
		list.add(buildNavButton(
			"User Management", ""
		));
		list.add(buildNavButton(
			"Contact Functions", ""
		));
		
		return list;
		
	}

	@Override
	protected String getListViewName() {
		return Lang.t("parameters");
	}


}
