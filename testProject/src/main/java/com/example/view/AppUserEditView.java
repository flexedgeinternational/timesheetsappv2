package com.example.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.example.bean.PasswordChangeBean;
import com.example.localization.Localizer;
import com.example.model.AppUser;
import com.example.model.AppUserRole;
import com.example.model.Role;
import com.example.repository.AppUserRoleRepository;
import com.example.service.crud.AppUserService;
import com.example.service.crud.EntityService;
import com.example.service.crud.RoleService;
import com.example.vaadin.core.window.GenericEditWindow;
import com.example.window.EditPasswordWindow;
import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@SpringView(name = "AppUserEditView")
public class AppUserEditView extends com.example.vaadin.core.view.AbstractEditView<AppUser> {

	@PropertyId("username")
	protected TextField username;

	@PropertyId("firstname")
	protected TextField firstname;

	@PropertyId("lastname")
	protected TextField lastname;

	@PropertyId("email")
	protected TextField email;

	@PropertyId("password")
	protected PasswordField password;

	@PropertyId("passwordConf")
	protected PasswordField passwordConf;

	@PropertyId("active")
	protected CheckBox active;

	protected Button changePassword;

	protected Grid roleGrid;

	@Autowired
	@Qualifier("appUserService")
	AppUserService customService;

	@Autowired
	RoleService roleService;

	@Autowired
	AppUserRoleRepository appUserRoleRepository;

	protected boolean valide = false;

	@Override
	protected void initService() {
		this.service = customService;
	}

	@Override
	protected void initFormFields() {

		firstname = new TextField(Localizer.getLocalizedTerm("firstname"));
		firstname.setNullRepresentation("");

		lastname = new TextField(Localizer.getLocalizedTerm("lastname"));
		lastname.setNullRepresentation("");

		username = new TextField(Localizer.getLocalizedTerm("username"));
		username.setNullRepresentation("");

		email = new TextField(Localizer.getLocalizedTerm("email"));
		email.setNullRepresentation("");

		password = new PasswordField(Localizer.getLocalizedTerm("password"));
		password.setNullRepresentation("");
		password.setVisible(true);

		passwordConf = new PasswordField(Localizer.getLocalizedTerm("PasswordConfirmation"));
		passwordConf.setNullRepresentation("");
		passwordConf.setVisible(true);

		passwordConf.addValidator(new Validator() {
			@Override
			public void validate(Object value) throws InvalidValueException {
				if (!(value instanceof String && ((String) value).equals(password.getValue())))
					throw new InvalidValueException("not compatible");
				else {
					valide = true;
				}
			}
		});

		active = new CheckBox(Localizer.getLocalizedTerm("active"));
		formLayout.addComponent(active);

		changePassword = new Button(Localizer.getLocalizedTerm("ChangePassword"));
		changePassword.setVisible(false);

		roleGrid = new Grid();
		roleGrid.setSelectionMode(SelectionMode.MULTI);
		roleGrid.setSizeFull();
		roleGrid.setHeightMode(HeightMode.ROW);
		roleGrid.setHeightByRows(roleService.getListObjects().size());
		roleGrid.setColumns("name");
		roleGrid.getColumn("name").setHeaderCaption(Localizer.getLocalizedTerm("Roles"));
		reloadGridRole(roleGrid);

		formLayout.setMargin(true);
		formLayout.setSpacing(true);
		formLayout.addComponents(username, password, passwordConf, firstname, lastname, email, active, changePassword,
				roleGrid);

		binder.bind(firstname, "firstname");
		binder.bind(lastname, "lastname");
		binder.bind(username, "username");
		binder.bind(email, "email");
		binder.bind(active, "active");

	}

	protected void reloadGridRole(Grid roleGrid) {
		BeanItemContainer<Role> container = new BeanItemContainer<>(roleService.getEntityClass(),
				roleService.getListObjects());
		roleGrid.setContainerDataSource(container);
	}

	@Override
	protected Class<AppUser> getEntityClass() {
		return AppUser.class;
	}

	@Override
	protected void updateEditActionVisibility(AppUser currentObject, String id) {
		super.updateEditActionVisibility(currentObject, id);

		if (!id.equals("")) {
			password.setVisible(false);
			passwordConf.setVisible(false);
			changePassword.setVisible(true);
			changePassword.addClickListener(e -> changePassword());

		} else {
			if (password.getValue().equals(passwordConf.getValue())) {
				binder.bind(password, "password");
			}
		}

	}

	@Override
	protected void saveListner(EntityService<AppUser> service, GenericEditWindow window, BeanFieldGroup<AppUser> binder,
			AppUser currentObject, String getViewTitle, boolean editWindow) {
		if (valide) {
			super.saveListner(service, null, binder, currentObject, getViewTitle, false);
		}
		AppUserRole appUserRole = null;

		for (Object role : roleGrid.getSelectedRows()) {
			if (appUserRoleRepository.findByAppUserAndRole(currentObject, (Role) role).isEmpty()) {
				appUserRole = new AppUserRole();
				appUserRole.setAppUser(currentObject);
				appUserRole.setRole((Role) role);
				appUserRoleRepository.save(appUserRole);
			}
		}

	}

	private void changePassword() {
		// Window to change Password of AppUser
		EditPasswordWindow changePasswordWindow = new EditPasswordWindow(customService, binder,
				Localizer.getLocalizedTerm("ChangePassword"));
		changePasswordWindow.setCancelButtonListener(cancelEvent -> changePasswordCancelButton(changePasswordWindow));
		changePasswordWindow.setSaveButtonListener(saveEvent -> changePasswordSaveButton(changePasswordWindow));
		UI.getCurrent().addWindow(changePasswordWindow);

	}

	private void changePasswordSaveButton(EditPasswordWindow changePasswordWindow) {
		// Save changePassword
		PasswordChangeBean bean = null;
		if (changePasswordWindow.valide) {
			bean = new PasswordChangeBean(currentObject, changePasswordWindow.password.getValue(),
					changePasswordWindow.passwordConf.getValue());
			customService.saveChangedPassword(bean);
			changePasswordWindow.close();
		}
	}

	private void changePasswordCancelButton(EditPasswordWindow changePasswordWindow) {
		binder.discard();
		changePasswordWindow.close();

	}
}
