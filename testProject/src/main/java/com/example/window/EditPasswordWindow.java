package com.example.window;

import com.example.localization.Localizer;
import com.example.vaadin.core.window.GenericEditWindow;
import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.ui.PasswordField;

public class EditPasswordWindow extends GenericEditWindow {

	@PropertyId("password")
	public PasswordField password;

	@PropertyId("passwordConf")
	public PasswordField passwordConf;

	public boolean valide = false;

	public EditPasswordWindow(Object currentObject, BeanFieldGroup<? extends Object> binder, String title) {
		super(currentObject, binder, title);

	}

	@Override
	protected void initFields() {
		super.initFields();
		password = new PasswordField(Localizer.getLocalizedTerm("password"));
		password.setNullRepresentation("");

		passwordConf = new PasswordField(Localizer.getLocalizedTerm("PasswordConfirmation"));
		passwordConf.setNullRepresentation("");
		passwordConf.addValidator(new Validator() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void validate(Object value) throws InvalidValueException {
				valide = false;
				if (!(value instanceof String && ((String) value).equals(password.getValue())))
					throw new InvalidValueException(Localizer.getLocalizedTerm("notCompatible"));
				else {
					valide = true;
				}
			}
		});

		addComponent(password);
		addComponent(passwordConf);
	}
}
