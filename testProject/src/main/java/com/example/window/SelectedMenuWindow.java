package com.example.window;

import java.util.ArrayList;
import java.util.List;

import com.example.model.MenuItem;
import com.example.vaadin.core.window.GenericEditWindow;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.ui.CheckBox;

public class SelectedMenuWindow extends GenericEditWindow {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected List<MenuItem> nonAffectedItemsMenu;
	protected CheckBox menuItemCheck;
	public List<CheckBox> menuItemCheckList = new ArrayList<>();

	public SelectedMenuWindow(Object currentObject, BeanFieldGroup<? extends Object> binder, String title,
			List<MenuItem> nonAffectedItemsMenu) {
		super(currentObject, binder, title);
		this.nonAffectedItemsMenu = nonAffectedItemsMenu;
		initContentField();
	}

	@Override
	protected void initFields() {
		super.initFields();
	}

	private void initContentField() {

		for (MenuItem menuItem : nonAffectedItemsMenu) {
			menuItemCheck = new CheckBox(menuItem.getMenuTitle());
			addComponent(menuItemCheck);
			menuItemCheckList.add(menuItemCheck);
		}

	}
}
