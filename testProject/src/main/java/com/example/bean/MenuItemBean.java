package com.example.bean;

import java.util.List;

public class MenuItemBean {

	private Integer oid;

	private MenuItemBean parent;

	private String title;

	private String viewName;

	private String faIconName;

	private List<MenuItemBean> children;

	public MenuItemBean(Integer oid, String title, String viewName, String faIconName) {
		super();
		this.oid = oid;
		this.title = title;
		this.viewName = viewName;
		this.faIconName = faIconName;
	}

	public MenuItemBean(Integer oid, MenuItemBean parent, String title, String viewName, String faIconName) {
		super();
		this.oid = oid;
		this.parent = parent;
		this.title = title;
		this.viewName = viewName;
		this.faIconName = faIconName;
	}

	public MenuItemBean(Integer oid) {
		super();
		this.oid = oid;
	}

	public MenuItemBean() {
		// TODO Auto-generated constructor stub
	}

	public Integer getOid() {
		return oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	public MenuItemBean getParent() {
		return parent;
	}

	public void setParent(MenuItemBean parent) {
		this.parent = parent;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	public String getFaIconName() {
		return faIconName;
	}

	public void setFaIconName(String faIconName) {
		this.faIconName = faIconName;
	}

	public List<MenuItemBean> getChildren() {
		return children;
	}

	public void setChildren(List<MenuItemBean> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return title;
	}
}
