package com.example.bean;

import javax.validation.constraints.NotNull;

import com.example.model.AppUser;

public class PasswordChangeBean {

	private Integer oid;

	private AppUser appUser;

	@NotNull
	private String password;

	@NotNull
	private String passwordConf;

	public PasswordChangeBean(Integer oid, AppUser appUser, String password, String passwordConf) {
		super();
		this.oid = oid;
		this.appUser = appUser;
		this.password = password;
		this.passwordConf = passwordConf;
	}

	public PasswordChangeBean(AppUser appUser, String password, String passwordConf) {
		super();
		this.appUser = appUser;
		this.password = password;
		this.passwordConf = passwordConf;
	}

	public Integer getOid() {
		return oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	public AppUser getAppUser() {
		return appUser;
	}

	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConf() {
		return passwordConf;
	}

	public void setPasswordConf(String passwordConf) {
		this.passwordConf = passwordConf;
	}

}
