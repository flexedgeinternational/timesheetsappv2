package com.example;

import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.example.localization.Localizer;
import com.example.model.AppUser;
import com.example.service.security.SecurityService;
import com.example.service.util.MenuService;
import com.example.vaadin.event.ApplicationEvent.CloseOpenWindowsEvent;
import com.example.vaadin.event.ApplicationEvent.UserLoggedOutEvent;
import com.example.vaadin.event.ApplicationEvent.UserLoginRequestedEvent;
import com.example.vaadin.event.ApplicationEventBus;
import com.example.view.MainView;
import com.example.view.security.LoginView;
import com.google.common.eventbus.Subscribe;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.WrappedHttpSession;
import com.vaadin.server.WrappedSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
@Theme("sample")
@SpringUI
public class MainUI extends UI {

	protected static final Logger log = LoggerFactory.getLogger(MainUI.class);

	private SpringViewProvider viewProvider;

	private final ApplicationEventBus applicationEventbus = new ApplicationEventBus();

	private ApplicationContext applicationContext;

	private AuthenticationManager authenticationManager;

	private SecurityService securityService;

	private MainView mainView;

	@Autowired
	private MenuService menuService;

	@Autowired
	public MainUI(SpringViewProvider viewProvider, AuthenticationManager authenticationManager,
			ApplicationContext applicationContext, SecurityService securityService) {
		this.viewProvider = viewProvider;
		this.authenticationManager = authenticationManager;
		this.applicationContext = applicationContext;
		this.securityService = securityService;
		Localizer.setLocale(new Locale("fr", "CA"));
	}

	@Override
	protected void init(VaadinRequest request) {
		WrappedSession session = request.getWrappedSession();
		HttpSession httpSession = ((WrappedHttpSession) session).getHttpSession();
		ServletContext servletContext = httpSession.getServletContext();
		applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
		ApplicationEventBus.register(this);
		Responsive.makeResponsive(this);
		updateContent();
	}

	/**
	 * Updates the correct content for this UI based on the current user status.
	 * If the user is logged in with appropriate privileges, main view is shown.
	 * Otherwise login view is shown.
	 */
	private void updateContent() {
		AppUser appUser = (AppUser) VaadinSession.getCurrent().getAttribute(AppUser.class.getName());
		if (appUser != null) {
			mainView = new MainView(menuService);
			ComponentContainer content = new CssLayout();
			content.addStyleName("view-content");
			content.setSizeFull();
			mainView.addComponent(content);
			mainView.setExpandRatio(content, 1.0f);
			Navigator nav = new Navigator(UI.getCurrent(), content);
			nav.addProvider(viewProvider);
			// Authenticated user
			setContent(mainView);
			removeStyleName("loginview");
			if (nav.getState() != null && !nav.getState().isEmpty()) {
				nav.navigateTo(nav.getState());
			} else {
				
				String role = securityService.getRole(appUser.getUsername());
				
		        switch (role) {
		            case "Vendor":  nav.navigateTo("VendorHomeView");
		                     break;
		            case "Resource":	
		            	nav.navigateTo("ResourceHomeView");
		                     break;
		            case "Admin":  		
		            	nav.navigateTo("HomeView");
		                     break;
		            default: 			
		            	nav.navigateTo("HomeView");
		                     break;
		        }
			}
		} else {
			LoginView loginView = new LoginView();
			setContent(loginView);
			addStyleName("loginview");
		}
	}

	public static ApplicationEventBus getApplicationEventbus() {
		return ((MainUI) getCurrent()).applicationEventbus;
	}

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	@Subscribe
	public void userLoginRequested(final UserLoginRequestedEvent event) {
		Authentication authentication = new UsernamePasswordAuthenticationToken(event.getUserName(),
				event.getPassword());

		if (loginUser(authentication)) {
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			AppUser appUser = securityService.retrieveAppUserByUsername(((User) principal).getUsername());
			UI.getCurrent().getSession().setAttribute(AppUser.class.getName(), appUser);
		}
		updateContent();
	}

	public boolean loginUser(Authentication authenticationRequest) {
		try {
			final Authentication authentication = authenticationManager.authenticate(authenticationRequest);
			if (authentication.isAuthenticated()) {
				SecurityContextHolder.getContext().setAuthentication(authentication);
				return true;
			}
		} catch (AuthenticationException aExc) {
			log.error(aExc.getMessage());
		}
		Notification notification = new Notification(
				Localizer.getLocalizedTerm(
						"Could not authenticate, The Username / Password Combination Provided is Incorrect"),
				Type.ERROR_MESSAGE);
		notification.setPosition(Position.BOTTOM_CENTER);
		notification.setDelayMsec(5000);
		notification.show(Page.getCurrent());
		return false;
	}

	@Subscribe
	public void userLoggedOut(final UserLoggedOutEvent event) {
		VaadinSession.getCurrent().close();
		Page.getCurrent().reload();
	}

	@Subscribe
	public void closeOpenWindows(final CloseOpenWindowsEvent event) {
		for (Window window : getWindows()) {
			window.close();
		}
	}

}
