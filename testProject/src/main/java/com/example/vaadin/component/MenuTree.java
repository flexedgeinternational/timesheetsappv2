package com.example.vaadin.component;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.example.bean.MenuItemBean;
import com.example.vaadin.menu.MenuItemSelectedCallBack;
import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Tree;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
public class MenuTree extends Tree {

	private HierarchicalContainer hierarchicalContainer;

	public MenuTree(List<MenuItemBean> items, MenuItemSelectedCallBack callBack) {
		// setStyleName("menu-side");
		hierarchicalContainer = new HierarchicalContainer();
		hierarchicalContainer.addContainerProperty("title", String.class, "");
		hierarchicalContainer.addContainerProperty("faIcon", FontAwesome.class, FontAwesome.SQUARE);
		this.addRootMenuItems(items);
		setSizeFull();
		setContainerDataSource(hierarchicalContainer);
		setItemCaptionPropertyId("title");
		setItemCaptionMode(ItemCaptionMode.PROPERTY);
		setItemIconPropertyId("faIcon");
		addItemClickListener(event -> {
			MenuItemBean item = (MenuItemBean) event.getItemId();
			callBack.onMenuItemSelection(item);
			if (getChildren(item) != null && getChildren(item).size() > 0) {
				if (isExpanded(item)) {
					collapseItem(item);
				} else {
					expandItem(item);
				}
			}
			if (StringUtils.isNotBlank(item.getViewName())) {
				UI.getCurrent().getNavigator().navigateTo(item.getViewName());
			}

		});

		addCollapseListener(event -> {
			MenuTree tree = (MenuTree) event.getSource();
			tree.setItemIcon(event.getItemId(), FontAwesome.PLUS_SQUARE);
		});

		addExpandListener(event -> {
			MenuTree tree = (MenuTree) event.getSource();
			tree.setItemIcon(event.getItemId(), FontAwesome.MINUS_SQUARE);
		});

	}

	private void addRootMenuItems(List<MenuItemBean> items) {

		for (MenuItemBean menuItem : items) {
			addItems(menuItem, null);
		}
	}

	@SuppressWarnings({ "unchecked" })
	private void addItems(MenuItemBean menuItem, MenuItemBean parentMenuItem) {
		Item item = hierarchicalContainer.addItem(menuItem);
		if (parentMenuItem != null) {
			hierarchicalContainer.setParent(menuItem, parentMenuItem);
		}
		item.getItemProperty("title").setValue(menuItem.getTitle());
		if (menuItem.getChildren() != null && !menuItem.getChildren().isEmpty()) {
			hierarchicalContainer.setChildrenAllowed(menuItem, true);
			item.getItemProperty("faIcon").setValue(FontAwesome.PLUS_SQUARE);
			// add children items
			for (MenuItemBean child : menuItem.getChildren()) {
				addItems(child, menuItem);
			}

		} else {
			hierarchicalContainer.setChildrenAllowed(menuItem, false);
			if (menuItem.getFaIconName() != null) {
				item.getItemProperty("faIcon").setValue(FontAwesome.valueOf(menuItem.getFaIconName()));
			}
		}
	}
}
