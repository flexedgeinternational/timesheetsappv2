package com.example.vaadin.component;

import java.io.File;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.util.CellReference;

import com.vaadin.addon.spreadsheet.Spreadsheet;

@SuppressWarnings("serial")
public class CSpreadsheet extends Spreadsheet {
	
    public CSpreadsheet() {	super(); }
    
    public CSpreadsheet(File file) throws IOException { super(file); }
	
    public Cell getOrCreateCell(CellReference cellRef) {
        Cell cell = this.getCell(cellRef.getRow(), cellRef.getCol());
        if (cell == null) {
            cell = this.createCell(cellRef.getRow(), cellRef.getCol(), "");
        }
        return cell;
    }
    
    public Cell getOrCreateCell(int row, int col) {
        Cell cell = this.getCell(row, col);
        if (cell == null) {
            cell = this.createCell(row, col, "");
        }
        return cell;
    }
    
    /*
    public Cell getOrCreateCell(String cellRef) {
        Cell cell = this.getCell(cellRef);
        if (cell == null) {
            cell = this.createCell(cellRef.getRow(), cellRef.getCol(), "");
        }
        return cell;
    }
    */
}
