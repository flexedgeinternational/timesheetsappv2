package com.example.vaadin.component;

import java.util.List;

import com.example.MainUI;
import com.example.bean.MenuItemBean;
import com.example.localization.Localizer;
import com.example.model.AppUser;
import com.example.service.util.MenuService;
import com.example.vaadin.event.ApplicationEvent.ProfileUpdatedEvent;
import com.example.vaadin.event.ApplicationEvent.UserLoggedOutEvent;
import com.example.vaadin.event.ApplicationEventBus;
import com.example.vaadin.menu.MenuItemSelectedCallBack;
import com.google.common.base.Strings;
import com.google.common.eventbus.Subscribe;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings({ "serial" })
@UIScope
public final class DashboardMenu extends CustomComponent {

	public static final String ID = "base-menu";
	public static final String SERVICE_BADGE_ID = "base-menu-service-badge";
	private static final String STYLE_VISIBLE = "valo-menu-visible";

	private MenuItem settingsItem;

	final CssLayout menuContent = new CssLayout();

	private MenuService menuService;

	public DashboardMenu(MenuService menuService) {
		MainUI currentUI = (MainUI) UI.getCurrent();

		if (currentUI != null) {
			this.menuService = menuService;
			setPrimaryStyleName("valo-menu");
			setId(ID);
			setSizeUndefined();

			// There's only one ApplicationMenu per UI so this doesn't need to
			// be
			// unregistered from the UI-scoped DashboardEventBus.
			// ApplicationEventBus.register(this);

			setCompositionRoot(buildContent());
		}
	}

	private Component buildContent() {
		final CssLayout menuContent = new CssLayout();
		menuContent.addStyleName("sidebar");
		menuContent.addStyleName(ValoTheme.MENU_PART);
		menuContent.addStyleName("no-vertical-drag-hints");
		menuContent.addStyleName("no-horizontal-drag-hints");
		menuContent.setWidth(null);
		menuContent.setHeight("100%");

		menuContent.addComponent(buildTitle());
		menuContent.addComponent(buildUserMenu());
		menuContent.addComponent(buildToggleButton());
		menuContent.addComponent(buildMenuItems());

		return menuContent;
	}

	private Component buildTitle() {
		Label logo = new Label(("Sample Application"));
		logo.setSizeUndefined();
		HorizontalLayout logoWrapper = new HorizontalLayout(logo);
		logoWrapper.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
		logoWrapper.addStyleName("valo-menu-title");
		return logoWrapper;
	}

	private AppUser getCurrentUser() {
		if (VaadinSession.getCurrent() != null) {
			return (AppUser) VaadinSession.getCurrent().getAttribute(AppUser.class.getName());
		}
		return null;
	}

	private Component buildUserMenu() {
		final MenuBar settings = new MenuBar();
		settings.addStyleName("user-menu");
		final AppUser user = getCurrentUser();
		if (user != null) {
			settingsItem = settings.addItem("", new ThemeResource("img/profile-pic-300px.jpg"), null);
			updateUserName(null);
			settingsItem.addItem(Localizer.getLocalizedTerm("Edit Profile"), new Command() {
				@Override
				public void menuSelected(final MenuItem selectedItem) {
					// ProfilePreferencesWindow.open(user, false);
				}
			});
			settingsItem.addItem(Localizer.getLocalizedTerm("Change Password"), new Command() {
				@Override
				public void menuSelected(final MenuItem selectedItem) {
					// ProfilePreferencesWindow.open(user, true);
				}
			});
			settingsItem.addSeparator();
			settingsItem.addItem("Sign Out", new Command() {
				@Override
				public void menuSelected(final MenuItem selectedItem) {
					ApplicationEventBus.post(new UserLoggedOutEvent());
				}
			});
		}
		return settings;
	}

	private Component buildToggleButton() {
		Button valoMenuToggleButton = new Button("Menu", new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				if (getCompositionRoot().getStyleName().contains(STYLE_VISIBLE)) {
					getCompositionRoot().removeStyleName(STYLE_VISIBLE);
				} else {
					getCompositionRoot().addStyleName(STYLE_VISIBLE);
				}
			}
		});
		valoMenuToggleButton.setIcon(FontAwesome.LIST);
		valoMenuToggleButton.addStyleName("valo-menu-toggle");
		valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_SMALL);
		return valoMenuToggleButton;
	}

	private Component buildMenuItems() {
		List<MenuItemBean> menuItems = menuService.retrieveMenuItems(getCurrentUser(), null);
		MenuTree menuTree = new MenuTree(menuItems, new MenuItemSelectedCallBack() {

			@Override
			public void onMenuItemSelection(MenuItemBean item) {
				if (!Strings.isNullOrEmpty(item.getViewName())) {
					getUI().getSession().setAttribute("menuviewname", item.getViewName());
				}

			}
		});
		return menuTree;

	}

	@Subscribe
	public void updateUserName(final ProfileUpdatedEvent event) {
		AppUser user = getCurrentUser();
		if (user != null) {
			settingsItem.setText(user.getFirstname() + " " + user.getLastname());
		}
	}

}
