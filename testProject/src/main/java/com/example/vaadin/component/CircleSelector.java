package com.example.vaadin.component;

import com.vaadin.server.Page;
import com.vaadin.server.Page.Styles;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

import java.lang.Math;
import java.util.ArrayList;

import org.vaadin.jouni.animator.Animator;
import org.vaadin.jouni.dom.client.Css;

@SuppressWarnings("serial")
public class CircleSelector extends VerticalLayout {

	Styles styles;
	
	public CircleSelector(ArrayList<Component> arrList) {
		Component[] arr = new Component[arrList.size()];
		arr = arrList.toArray(arr);
		initComponent(arr);
	}
	
	public CircleSelector(Component[] compArr, Css css)	{
		initComponent(compArr);
		animate(css);
	}
	
	/* If no css is specified use the default styling provided */
	public CircleSelector(Component[] compArr) {
		initComponent(compArr);
	}
	
	private void initComponent(Component[] compArr) {
		setId("circleSelector");
		setSizeFull();
	    styles = Page.getCurrent().getStyles();
	    
	   	Component buttonCircle = buildButtonCircle(compArr);
		addComponent(buttonCircle);
		setComponentAlignment(buttonCircle, Alignment.BOTTOM_CENTER);	
	}
	
	private void animate(Css css) {
		Animator.animate(
			(AbstractComponent) this
			, css
		).duration(1000);
	}
	
	private Component buildButtonCircle(Component[] compArr) {
		
		HorizontalLayout layout = new HorizontalLayout();
		layout.setStyleName("circle-container");

		int n = compArr.length-1;
		
		/* First one gets no styling, and therefore appears in the middle */
		layout.addComponent(compArr[0]);
		
		/* Skip first (centered) element and position others in circle */
		for(int i = 0; i < n; i++) {
			
			double y = 0;
			double x = 0;
			int R = 12;
			
			/* basic trig ... unit circle and all that jazz */
			x = R * Math.cos(i*2*Math.PI/n);
			y = R * Math.sin(i*2*Math.PI/n);
			
			/* convert x,y coordinates into degrees */
			double rad = Math.atan2(x, y); 
			int intDeg = (int)Math.ceil(rad * (180 / Math.PI));
			String strDeg = Integer.toString(intDeg);
			
			String styleName = "deg"+strDeg;

			/* rotation == (-1)*x so the elements appears right-side-up*/
			String styleString = 
				"." + styleName 
				+ " {transform: rotate("+ strDeg +"deg) translate(12em) rotate("+ Integer.toString((-1)*intDeg) +"deg); }";
			
			styles.add(styleString);
			
			/* Dont reference the first item, as it is the center item */
			int elementReference = i+1;
			
			compArr[elementReference].addStyleName(styleName);
			
			layout.addComponent(compArr[elementReference]);
		    
		}
		return layout;
	}
}

