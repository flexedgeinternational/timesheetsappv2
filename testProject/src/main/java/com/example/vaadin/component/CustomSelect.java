package com.example.vaadin.component;

import java.util.Iterator;
import java.util.List;
import com.vaadin.ui.ComboBox;

@SuppressWarnings("serial")
public class CustomSelect extends ComboBox {

	public CustomSelect(String t) {
		super(t);
		this.setNullSelectionAllowed(false);
	}

	public void populate_objArrList(List<Object[]> strArrList) {
		for(Iterator<Object[]> i = strArrList.iterator(); i.hasNext(); ) {

			Object[] items = i.next();
			    
		    int id = Integer.parseInt(items[0].toString());
		    String val = items[1].toString();
			
			this.addItem(id);
			this.setItemCaption(id, val);
		}
	}
	
	public void populate_strList(List<String> strList) {
		for(Iterator<String> i = strList.iterator(); i.hasNext(); ) {

			String item = i.next();
			
			this.addItem(item);
			this.setItemCaption(item, item);
		}
	}
	
	public void set(int id) {
		
		//this.getItem("2");
		
		this.select("2");
	}
	
}
