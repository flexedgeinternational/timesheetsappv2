package com.example.vaadin.event;

import com.example.MainUI;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;

public class ApplicationEventBus implements SubscriberExceptionHandler {
	private final EventBus eventBus = new EventBus(this);

	public static void post(final Object event) {
		MainUI.getApplicationEventbus().eventBus.post(event);
	}

	public static void register(final Object object) {
		MainUI.getApplicationEventbus().eventBus.register(object);
	}

	public static void unregister(final Object object) {
		MainUI.getApplicationEventbus().eventBus.unregister(object);
	}

	@Override
	public final void handleException(final Throwable exception, final SubscriberExceptionContext context) {
		exception.printStackTrace();
	}
}
