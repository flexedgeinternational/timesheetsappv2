package com.example.vaadin.event;

import com.example.bean.MenuItemBean;

/*
 * Event bus events used in Dashboard are listed here as inner classes.
 */
public abstract class ApplicationEvent {

	public static final class UserLoginRequestedEvent {
		private final String userName, password;

		public UserLoginRequestedEvent(final String userName, final String password) {
			this.userName = userName;
			this.password = password;
		}

		public String getUserName() {
			return userName;
		}

		public String getPassword() {
			return password;
		}
	}

	public static class BrowserResizeEvent {

	}

	public static class UserLoggedOutEvent {

	}

	public static class CloseOpenWindowsEvent {
	}

	public static class ProfileUpdatedEvent {
	}

	public static final class PostMenuItemChangeEvent {
		private final MenuItemBean menuItem;

		public PostMenuItemChangeEvent(final MenuItemBean menuItem) {
			this.menuItem = menuItem;
		}

		public MenuItemBean getMenuItem() {
			return menuItem;
		}
	}

}
