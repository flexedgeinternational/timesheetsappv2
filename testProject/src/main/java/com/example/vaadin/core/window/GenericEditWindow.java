package com.example.vaadin.core.window;

import com.example.localization.Localizer;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
public class GenericEditWindow extends Window {

	protected VerticalLayout mainLayout;

	protected HorizontalLayout headerLayout;

	protected HorizontalLayout footerLayout;

	protected FormLayout formLayout;

	protected Label titleLabel;

	protected Object currentObject;

	protected Button saveButton;

	protected Button cancelButton;

	protected final BeanFieldGroup<? extends Object> binder;

	public GenericEditWindow(Object currentObject, BeanFieldGroup<? extends Object> binder, String title) {
		this.currentObject = currentObject;
		this.binder = binder;
		initMainLayout(title);
		initFields();
	}

	protected void initMainLayout(String title) {
		center();
		setWidth(30.0f, Unit.PERCENTAGE);
		mainLayout = new VerticalLayout();
		initHeader(title);
		initContent();
		initFooter();
		setContent(mainLayout);
	}

	protected void initHeader(String title) {
		headerLayout = new HorizontalLayout();
		headerLayout.addStyleName("viewheader");
		headerLayout.setSpacing(true);
		headerLayout.setMargin(true);
		Responsive.makeResponsive(headerLayout);

		titleLabel = new Label(Localizer.getLocalizedTerm(title));
		titleLabel.setSizeUndefined();
		titleLabel.addStyleName(ValoTheme.LABEL_H1);
		titleLabel.addStyleName(ValoTheme.LABEL_NO_MARGIN);
		headerLayout.addComponent(titleLabel);
		mainLayout.addComponent(headerLayout);
	}

	protected void initFooter() {
		footerLayout = new HorizontalLayout();
		footerLayout.setSpacing(true);
		footerLayout.setMargin(true);
		initFooterContent();
		mainLayout.addComponent(footerLayout);
	}

	public void initContent() {
		formLayout = new FormLayout();
		formLayout.setMargin(true);
		formLayout.setSpacing(true);
		mainLayout.addComponent(formLayout);
		mainLayout.setExpandRatio(formLayout, 1);
	}

	protected void initFooterContent() {
		saveButton = new Button(Localizer.getLocalizedTerm("Save"), FontAwesome.SAVE);

		saveButton.addClickListener(e -> {
			try {
				binder.commit();
			} catch (Exception e1) {
				Notification.show(Localizer.getLocalizedTerm("There are validation errors"));
			}
		});

		cancelButton = new Button(Localizer.getLocalizedTerm("Cancel"), FontAwesome.BACKWARD);

		cancelButton.addClickListener(e -> {
			binder.discard();
		});

		footerLayout.addComponent(saveButton);
		footerLayout.addComponent(cancelButton);
	}

	public void setSaveButtonListener(ClickListener clickListener) {
		this.saveButton.addClickListener(clickListener);
	}

	public void setCancelButtonListener(ClickListener clickListener) {
		this.cancelButton.addClickListener(clickListener);
	}

	public void addField(Field field, Object propertyId) {
		binder.bind(field, propertyId);
		formLayout.addComponent(field);
	}

	public void addField(Field field) {
		formLayout.addComponent(field);
	}
	
	public void addComponent(Component component) {
		formLayout.addComponent(component);
	}

	protected void initFields() {
	}

	public BeanFieldGroup<? extends Object> getBinder() {
		return binder;
	}

}
