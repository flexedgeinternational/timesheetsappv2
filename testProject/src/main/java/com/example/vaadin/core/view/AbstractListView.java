package com.example.vaadin.core.view;

import java.util.Collection;
import java.util.HashMap;

import com.example.localization.Localizer;
import com.example.service.crud.EntityService;
import com.example.vaadin.core.window.GenericEditWindow;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Button;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.MultiSelectionModel;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public abstract class AbstractListView<T extends Object> extends AbstractBaseLayout<T> implements View {

	protected VerticalLayout contentLayout;

	protected HorizontalLayout actionsLayout;

	protected Button editButton;
	protected Button deleteButton;
	protected Button createButton;

	protected Grid listGrid;

	protected EntityService<T> service;

	protected int i = 0;

	protected HashMap<Object, Field<?>> listOfFieldFilter = new HashMap<>();

	protected String getEditViewName() {
		return service.getEntityClass().getSimpleName() + "EditView";
	}

	@Override
	protected void initContent() {
		contentLayout = new VerticalLayout();
		contentLayout.setSpacing(true);
		contentLayout.setMargin(true);
		contentLayout.setSizeFull();
		Responsive.makeResponsive(contentLayout);

		actionsLayout = new HorizontalLayout();
		actionsLayout.setSpacing(true);
		actionsLayout.setMargin(true);
		addComponent(actionsLayout);

		initEditButton();
		initDeleteButton();
		initCreateButton();
		initGrid();
		addComponent(contentLayout);
		setExpandRatio(contentLayout, 1);

	}

	protected void initCreateButton() {
		createButton = new Button(Localizer.getLocalizedTerm("New"), FontAwesome.PLUS);
		actionsLayout.addComponent(createButton);
		actionsLayout.setExpandRatio(createButton, 1);
		createButton.addClickListener(e -> {

			if (addEditInWindow()) {
				T row = service.createObject();
				addWindow(row);
			} else {
				navigator.navigateTo(getEditViewName());
			}

		});
	}

	protected void initEditButton() {
		editButton = new Button(Localizer.getLocalizedTerm("Edit"), FontAwesome.EDIT);
		actionsLayout.addComponent(editButton);
		editButton.setEnabled(false);
		editButton.addClickListener(e -> {

			Collection<Object> selectedRows = listGrid.getSelectedRows();
			if (selectedRows.size() == 1) {
				T row = (T) selectedRows.iterator().next();
				if (addEditInWindow()) {
					addWindow(row);
				} else {
					navigator.navigateTo(getEditViewName() + "/" + service.getSelectedRowId(row));
				}
			}
		});
	}

	protected void initDeleteButton() {
		deleteButton = new Button(Localizer.getLocalizedTerm("Delete"), FontAwesome.TRASH);
		actionsLayout.addComponent(deleteButton);
		deleteButton.setEnabled(false);
		deleteButton.addClickListener(e -> {
			Collection<Object> selectedRows = listGrid.getSelectedRows();
			if (!(selectedRows == null || selectedRows.isEmpty())) {
				service.deleteSelectedRows(selectedRows);
				reloadGridData();
			}
			((MultiSelectionModel) listGrid.getSelectionModel()).deselectAll();
		});
	}

	protected void initGrid() {
		listGrid = new Grid();
		// Activate multi selection mode
		listGrid.setSelectionMode(SelectionMode.MULTI);
		listGrid.setSizeFull();
		reloadGridData();
		listGrid.setColumns(visibleColumns());
		listGrid.addSelectionListener(e -> {
			Integer selectedRowSize = listGrid.getSelectedRows().size();
			deleteButton.setEnabled(selectedRowSize > 0);
			editButton.setEnabled(selectedRowSize == 1);
		});
		if (selectionItemGrid()) {
			listGrid.addItemClickListener(new ItemClickListener() {
				@Override
				public void itemClick(ItemClickEvent event) {
					itemGridClickListner((T) event.getItemId());
				}
			});
		}
		contentLayout.addComponent(listGrid);
		contentLayout.setExpandRatio(listGrid, 1);

	}

	@Override
	protected void reloadGridData() {
		BeanItemContainer<T> container = new BeanItemContainer<>(service.getEntityClass(), service.getListObjects());
		
		listGrid.setContainerDataSource(container);
		// Only If Filter is a textField
		if (addFilterRow()) {
			i = i + 1;
			HeaderRow filterRow = null;
			if (i == 1) {
				filterRow = listGrid.appendHeaderRow();

				// Set up a filter for all columns
				for (Object pid : listGrid.getContainerDataSource().getContainerPropertyIds()) {
					HeaderCell cell = null;
					if (filterRow != null) {
						cell = filterRow.getCell(pid);
					}
					// Have an input field to use for filter
					TextField filterField = new TextField();
					// filterField.setColumns(8);

					// Update filter When the filter input is changed
					filterField.addTextChangeListener(change -> {
						// Can't modify filters so need to replace
						container.removeContainerFilters(pid);

						// (Re)create the filter if necessary
						if (!change.getText().isEmpty())
							container.addContainerFilter(new SimpleStringFilter(pid, change.getText(), true, false));
					});
					listOfFieldFilter.put(pid, filterField);
					if ((filterField != null) && (cell != null)) {
						cell.setComponent(filterField);
					}

				}
			}
			for (Object pid : listOfFieldFilter.keySet()) {
				if (listOfFieldFilter.get(pid) != null) {
					((TextField) listOfFieldFilter.get(pid)).addTextChangeListener(change -> {
						// Can't modify filters so need to replace
						container.removeContainerFilters(pid);
						// (Re)create the filter if necessary
						if (!change.getText().isEmpty())
							container.addContainerFilter(new SimpleStringFilter(pid, change.getText(), true, false));
					});

				}
			}
		}
	}

	protected boolean addFilterRow() {
		return true;
	}

	protected boolean selectionItemGrid() {
		return false;
	}

	protected boolean addEditInWindow() {
		return true;
	}

	protected void itemGridClickListner(T t) {

		if (addEditInWindow()) {
			addWindow(t);
		}
	}

	@Override
	protected String getListViewName() {
		return service.getEntityClass().getSimpleName() + "ListView";
	}

	protected void addWindow(T row) {
		BeanFieldGroup<T> binder = new BeanFieldGroup<T>(getEntityClass());
		binder.setItemDataSource(row);
		GenericEditWindow window = new GenericEditWindow(row, binder, service.getEditViewTitle(row));
		initWindowEditFields(window);
		UI.getCurrent().addWindow(window);
		window.setSaveButtonListener(
				saveEvent -> saveListner(service, window, binder, row, getListViewName(), addEditInWindow()));
		window.setCancelButtonListener(cancelEvent -> cancelListner(window, binder, addEditInWindow()));
	}

	abstract protected Object[] visibleColumns();
}
