package com.example.vaadin.core.view;

import javax.annotation.PostConstruct;

import com.example.localization.Localizer;
import com.example.service.crud.EntityService;
import com.example.vaadin.core.window.GenericEditWindow;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Responsive;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
public abstract class AbstractBaseLayout<T extends Object> extends VerticalLayout {

	protected EntityService<T> service;

	protected HorizontalLayout headerLayout;

	protected HorizontalLayout footerLayout;

	protected String objectId;
	
	protected Navigator navigator = UI.getCurrent().getNavigator(); 
	
	@PostConstruct
	void init() {
		setSizeFull();
		initService();
		initHeader();
		initContent();
		initFooter();
		Responsive.makeResponsive(this);
	}

	protected void initHeader() {
		headerLayout = new HorizontalLayout();
		headerLayout.addStyleName("viewheader");
		headerLayout.setSpacing(true);
		headerLayout.setMargin(true);
		Responsive.makeResponsive(headerLayout);

		Label title = new Label(Localizer.getLocalizedTerm(getViewTitle()));
		title.setSizeUndefined();
		title.addStyleName(ValoTheme.LABEL_H1);
		title.addStyleName(ValoTheme.LABEL_NO_MARGIN);
		headerLayout.addComponent(title);
		addComponent(headerLayout);

	}

	protected void initFooter() {
		footerLayout = new HorizontalLayout();
		footerLayout.setSpacing(true);
		footerLayout.setMargin(true);
		initFooterContent();
		addComponent(footerLayout);
		Responsive.makeResponsive(footerLayout);
	}

	protected void cancelListner(GenericEditWindow window, BeanFieldGroup<T> binder, boolean editWindow) {
		binder.discard();
		if (editWindow) {
			window.close();
		} else {
			navigator.navigateTo(getListViewName());
		}

	}

	protected void saveListner(EntityService<T> service, GenericEditWindow window, BeanFieldGroup<T> binder,
			T currentObject, String getViewTitle, boolean editWindow) {

		try {
			binder.commit();
			service.saveObject(currentObject);

			if (editWindow) {
				window.close();
			} else {
				Notification.show(Localizer.getLocalizedTerm("Saved successfully!"));
				navigator.navigateTo(getListViewName());
			}
			reloadGridData();
		} catch (CommitException e) {
			Notification.show(Localizer.getLocalizedTerm("There are validation errors"));
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
			System.err.println(e.getLocalizedMessage());
		}
	}

	abstract protected void initService();

	abstract protected void initContent();

	abstract protected void initFooterContent();

	abstract protected String getViewTitle();

	abstract protected Class<T> getEntityClass();

	abstract protected String getListViewName();

	protected void reloadGridData() {

	}

	protected void initWindowEditFields(GenericEditWindow window) {

	}

}
