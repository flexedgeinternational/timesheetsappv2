package com.example.vaadin.core.view;

import com.example.localization.Localizer;
import com.example.service.crud.EntityService;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Panel;

@SuppressWarnings("serial")
public abstract class AbstractEditView<T extends Object> extends AbstractBaseLayout<T> implements View {

	protected EntityService<T> service;

	protected T currentObject;

	protected Panel formPanel;

	protected FormLayout formLayout;

	protected Button saveButton;

	protected Button cancelButton;

	protected final BeanFieldGroup<T> binder = new BeanFieldGroup<>(getEntityClass());

	@Override
	public void enter(ViewChangeEvent event) {
		objectId = event.getParameters();
		loadObject(objectId);
	}

	protected void loadObject(String id) {
		if (id == "" || objectId.isEmpty()) {
			currentObject = service.createObject();
		} else {
			currentObject = service.loadObject(Long.parseLong(id));
		}
		updateEditActionVisibility(currentObject, id);
		binder.setItemDataSource(currentObject);
		binder.bindMemberFields(formLayout);
		binder.setBuffered(true);

	}

	protected void initContent() {
		formLayout = new FormLayout();
		formLayout.setResponsive(true);
		formPanel = new Panel(formLayout);
		formPanel.setSizeFull();
		initFormFields();
		addComponent(formPanel);
		setExpandRatio(formPanel, 1);

	}

	protected void initFooterContent() {
		saveButton = new Button(Localizer.getLocalizedTerm("Save"), FontAwesome.SAVE);

		saveButton.addClickListener(e -> {
			saveListner(service, null, binder, currentObject, getListViewName(), false);
		});

		cancelButton = new Button(Localizer.getLocalizedTerm("Cancel"), FontAwesome.BACKWARD);

		cancelButton.addClickListener(e -> {
			cancelListner(null, binder, false);
		});

		footerLayout.addComponent(saveButton);
		footerLayout.addComponent(cancelButton);
	}

	protected String getViewTitle() {
		return service.getEditViewTitle(currentObject);
	}

	abstract protected void initService();

	abstract protected void initFormFields();

	@Override
	protected String getListViewName() {
		return service.getEntityClass().getSimpleName() + "ListView";
	}

	protected void updateEditActionVisibility(T currentObject, String id) {

	}

}
