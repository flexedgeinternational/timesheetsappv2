package com.example.vaadin.core.view;

import com.example.localization.Localizer;
import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public abstract class AbstractGraphView<T extends Object> extends AbstractBaseLayout<T> implements View {

	protected Long contractId;
	
	protected VerticalLayout contentLayout;
	
	protected Button cancelButton;
	
	protected Button[] list;
	
	protected VerticalLayout chartLayout;
	
	protected Component chart;
	
	protected void initContent() {
		setSizeUndefined();
		setWidth("100%");
		setSpacing(true);
		
		addComponent(buildHeader());
		
		chartLayout = new VerticalLayout();
		chartLayout.setStyleName("chartLayout");
		chartLayout.setMargin(true);
		addComponent(chartLayout);
		buildChart(getChart());
	}

	protected HorizontalLayout buildHeader() {
		HorizontalLayout hLayout = new HorizontalLayout();
		hLayout.addStyleName("graphviewheader");
		hLayout.setSpacing(true);
		hLayout.setMargin(true);

		for (Button buton : options()) {
			buton.setStyleName("graphheaderimage");
			hLayout.addComponent(buton);
		}
		return hLayout;
	}
	
	protected void buildChart(Component chart) {
		chartLayout.removeAllComponents();
		chartLayout.addComponent(chart);
	}
	
	protected void buildOptions() {
		list = options();
	}
	
	@Override
	protected void initFooterContent() {
		footerLayout.setStyleName("graphfooter");
		cancelButton = new Button(Localizer.getLocalizedTerm("cancel"), FontAwesome.BACKWARD);
		cancelButton.addClickListener(e -> {
			navigator.navigateTo(getPreviousView());
		});
		footerLayout.addComponent(cancelButton);
	}

	
	@Override
	protected String getListViewName() {
		return service.getEntityClass().getSimpleName() + "ListView";
	}

	@Override
	protected void initService() {	}
	
	protected abstract String getPreviousView();
	
	protected abstract Button[] options();
	
	protected abstract Component getChart();
	
}
