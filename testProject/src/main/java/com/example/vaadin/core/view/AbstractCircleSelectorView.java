package com.example.vaadin.core.view;

import java.util.ArrayList;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.example.vaadin.component.CircleSelector;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
public abstract class AbstractCircleSelectorView<T extends Object> 
	extends AbstractBaseLayout<T> 
	implements View 
{ 
	
	protected String currentTitle = "";
	
	protected CircleSelector circleSelector;
	
	protected Navigator navigator = UI.getCurrent().getNavigator();
	
	protected ArrayList<Component> list = new ArrayList<Component>();
	
	protected Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	
	@Override
	public void enter(ViewChangeEvent event) {
		//objectId = event.getParameters();
	}

	protected void buildCircleSelector() {
		
		/* If you are transfering to a new circle selector
		 * make sure the old one has been removed */
		this.removeAllComponents();
		initHeader();
		
		/* build circle selector based on state */
		circleSelector = new CircleSelector(options());
		circleSelector.setSpacing(true);
		circleSelector.setMargin(true);

		addComponent(circleSelector);
		
		initFooter();
	}
	
	/* These should be moved down to the types of urls that use them */
	protected Button buildNavButton(String title, FontAwesome icon, String path) {
		Button btn = new Button(title);
		btn.setIcon(icon);
		btn.addClickListener(e -> {
			navigator.navigateTo(path);
		});
		
		return btn; 
	}
	
	/* These should be moved down to the types of urls that use them */
	protected Button buildNavButton(String title, String path) {
		Button btn = new Button(title);
		btn.addClickListener(e -> {
			navigator.navigateTo(path);
		});
		
		return btn; 
	}
	
	@Override
	protected String getViewTitle() {
		return currentTitle;
	}
	
	@Override
	protected void initContent() {
		buildCircleSelector();
	}
	
	@Override
	protected Class<T> getEntityClass() {
		return null;
	}
	
	@Override
	protected void initFooterContent() {
	}
	
	@Override
	protected void initService() {}
	
	abstract protected ArrayList<Component> options();
	
} 
