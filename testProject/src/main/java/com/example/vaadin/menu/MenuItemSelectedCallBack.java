package com.example.vaadin.menu;

import com.example.bean.MenuItemBean;

public interface MenuItemSelectedCallBack {
	void onMenuItemSelection(MenuItemBean item);
}
