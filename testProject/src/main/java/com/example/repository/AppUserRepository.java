package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.example.model.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Integer>, JpaSpecificationExecutor<AppUser> {
	public AppUser findByUsername(String username);

	public AppUser findByUsernameAndPassword(String username, String password);

	@Query(value = "select  r.oid,r.name from app_user a  INNER join app_user_role aur on a.oid=aur.oid_app_user "
			+ "Inner join role r on aur.oid_role=r.oid where a.oid=?1", nativeQuery = true)
	public List<Object[]> findRolesByAppUser(Integer appUserOid);

	@Query(value = "SELECT name as role	FROM app_user JOIN app_user_role ON app_user.oid = app_user_role.oid_app_user JOIN role	ON role.oid = app_user_role.oid_role WHERE username LIKE ?1", nativeQuery = true)
	String getUserRole(String contractId);

	@Query(value = "SELECT app_user.* FROM app_user JOIN app_user_role ON app_user_role.oid_app_user = app_user.oid JOIN role ON role.oid = app_user_role.oid_role WHERE name like 'Resource'", nativeQuery = true)
	List<AppUser> findAllResources();
	
}

