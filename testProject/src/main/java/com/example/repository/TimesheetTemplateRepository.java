package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.TimesheetTemplate;

public interface TimesheetTemplateRepository extends JpaRepository<TimesheetTemplate, Long> {

	@Query(value = "SELECT * from timesheet_template WHERE \"_default\" = 1 LIMIT 1", nativeQuery = true)
	TimesheetTemplate getDefaultTemplate();
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE timesheet_template SET \"_default\" = 0 WHERE 1=1; UPDATE timesheet_template SET \"_default\" = 1 WHERE oid = ?1", nativeQuery = true)
	void setAsDefault(Long templateId);
		
	//getDefaultTemplate
	
}
