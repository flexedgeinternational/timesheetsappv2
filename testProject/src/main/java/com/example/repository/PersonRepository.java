package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.model.Person;

public interface PersonRepository extends JpaRepository<Person, Long> {

	@Query(value = "SELECT person.oid, CONCAT(first_name, ' ', last_name, ': ', company.name) FROM person JOIN company ON company.oid = person.company", nativeQuery = true)
	List<Object[]> getPeople();
	
}
