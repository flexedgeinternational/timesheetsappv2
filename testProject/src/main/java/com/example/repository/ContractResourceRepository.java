package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.example.model.ContractResource;

public interface ContractResourceRepository extends JpaRepository<ContractResource, Long> {

	//getContractResource
	
	@Query(value = "SELECT * FROM contract_resource JOIN app_user ON app_user.username = ?2 WHERE contract_resource.contract_oid = ?1 LIMIT 1", nativeQuery = true)
	ContractResource getContractResource(int contractId, String username);
	
}
