package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.example.model.Currency;

public interface CurrencyRepository  extends JpaRepository<Currency, String> {

	@Query(value = "SELECT type FROM currency", nativeQuery = true)
	List<String> getCurrencies();
	
}
