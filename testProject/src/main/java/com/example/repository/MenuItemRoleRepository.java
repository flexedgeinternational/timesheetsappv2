package com.example.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.example.model.MenuItem;
import com.example.model.MenuItemRole;
import com.example.model.Role;

public interface MenuItemRoleRepository extends JpaRepository<MenuItemRole, Integer> {

	List<MenuItemRole> findByMenuItem(MenuItem menuitem);

	List<MenuItemRole> findByRole(Role role);

	List<MenuItemRole> findByRoleAndMenuItem(Role role, MenuItem menuitem);

	@Modifying
	@Transactional
	@Query(value = "delete from menu_item_role where oid_menu_item = ?1 and oid_role=?2", nativeQuery = true)
	void deleteByMenuAndRole(Integer oidMenuItem, Integer oidRole);

}
