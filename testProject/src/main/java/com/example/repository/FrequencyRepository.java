package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.model.Frequency;

public interface FrequencyRepository  extends JpaRepository<Frequency, Long> {

	@Query(value = "SELECT oid, frequency FROM frequency", nativeQuery = true)
	List<Object[]> getFrequencies();
	
}
