package com.example.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.Timesheet;

public interface TimesheetRepository extends JpaRepository<Timesheet, Long> {

	@Query(value = "SELECT timesheet_totals.* FROM timesheet_totals JOIN app_user ON app_user.username = ?2 JOIN contract_resource ON contract_resource.resource_oid = app_user.oid WHERE contract_resource.contract_oid = ?1", nativeQuery = true)
	List<Timesheet> getListObjects(int contractId, String username);
	
	@Modifying
	@Transactional
	@Query(value = "INSERT INTO timesheet " 
		+ "(start_date, end_date, mon_reg, tues_reg, wed_reg, thurs_reg, fri_reg,	sat_reg, sun_reg, "
		+ "mon_ot, tues_ot,	wed_ot,	thurs_ot, fri_ot, sat_ot, sun_ot, "
		+ "contract_resource_oid, timesheet_oid, submitted)"
		+ " VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19)", nativeQuery = true)
	void customSave(
		Date start_date,
		Date end_date,
		double monReg,
		double tuesReg,
		double wedReg,
		double thursReg,
		double friReg,
		double satReg,
		double sunReg,
		double monOt,
		double tuesOt,
		double wedOt,
		double thursOt,
		double friOt,
		double satOt,
		double sunOt,
		long contract_resource_oid,
		long timesheet_template_oid,
		Date submitted
	);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE timesheet SET " 
	+ "start_date = ?1, end_date = ?2,"
	+ " mon_reg = ?3, tues_reg = ?4, wed_reg = ?5, thurs_reg = ?6, fri_reg = ?7, sat_reg = ?8, sun_reg = ?9,"
	+ "	mon_ot = ?10, tues_ot = ?11, wed_ot = ?12, thurs_ot = ?13, fri_ot = ?14, sat_ot = ?15, sun_ot = ?16,"
	+ "	contract_resource_oid = ?17, timesheet_oid = ?18,	submitted = ?19"
	+ " WHERE oid = ?20", nativeQuery = true)
	void customUpdate(
		Date start_date,
		Date end_date,
		double monReg,
		double tuesReg,
		double wedReg,
		double thursReg,
		double friReg,
		double satReg,
		double sunReg,
		double monOt,
		double tuesOt,
		double wedOt,
		double thursOt,
		double friOt,
		double satOt,
		double sunOt,
		long contract_resource_oid,
		long timesheet_template_oid,
		Date submitted,
		double oid
	);
	
	@Modifying
	@Transactional
	@Query(value = "DELETE FROM timesheet WHERE oid = ?1", nativeQuery = true)
	void customDelete(double oid);
	
}
