package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.Contract;

public interface ContractRepository extends JpaRepository<Contract, Long> {

	@Query(value = "SELECT * from \"tasks\" WHERE \"contract_id\" = ?1", nativeQuery = true)
	List<Object[]> findTasksByContractId(Integer contractId);
	
	@Query(value = "SELECT * from \"phase\" WHERE \"contract_id\" = ?1", nativeQuery = true)
	List<Object[]> findPhasesByContractId(Integer contractId);
	
	@Query(value = "SELECT client from Contract WHERE \"id\" = ?1 LIMIT 1", nativeQuery = true)
	String getClient(Integer contractId);
	
	@Query(value = "SELECT service_provider from Contract WHERE \"id\" = ?1 LIMIT 1", nativeQuery = true)
	String getServiceProvider(Integer contractId);
	
	@Query(value = "SELECT technical_auth from Contract WHERE \"id\" = ?1 LIMIT 1", nativeQuery = true)
	Long getTechAuth(Integer contractId);
	
	@Query(value = "SELECT contracting_auth from Contract WHERE \"id\" = ?1 LIMIT 1", nativeQuery = true)
	Long getContractingAuth(Integer contractId);
	
	@Query(value = "SELECT vendor_auth from Contract WHERE \"id\" = ?1 LIMIT 1", nativeQuery = true)
	Long getVendorAuth(Integer contractId);
	
	@Query(value = "SELECT id, concat(first_name, ' ' , last_name, ' - ', company) info FROM people", nativeQuery = true)
	List<Object[]> getPeople();
	
	@Query(value = "SELECT name FROM company", nativeQuery = true)
	List<String> getCompanies();
	
	@Query(value = "SELECT type FROM currency", nativeQuery = true)
	List<String> getCurrencies();
	
	@Query(value = "SELECT total_cost FROM contract where id = ?1", nativeQuery = true)
	Integer getTotalCost(Long contractId);
	
	@Query(value = "SELECT SUM(per_diem_to_client) FROM resource WHERE contract_id = ?1", nativeQuery = true)
	Integer getTotalDailyCost(Integer contractId);
	
	@Query(value = "SELECT contract_oid, contract_title, client FROM contract JOIN contract_resource ON contract_resource.contract_oid = contract.oid JOIN app_user ON app_user.oid = contract_resource.resource_oid WHERE app_user.username like ?1", nativeQuery = true)
	List<Object[]> getResourceContracts(String resourceName);
	
	@Query(value = "SELECT contract.* FROM contract JOIN contract_resource ON contract_resource.contract_oid = contract.oid JOIN app_user ON app_user.oid = contract_resource.resource_oid WHERE app_user.username like ?1 AND contract.status like 'Closed'", nativeQuery = true)
	List<Contract> getClosedContracts(String user);
	
	@Query(value = "SELECT * FROM contract WHERE status NOT LIKE 'Closed'", nativeQuery = true)
	List<Contract> findAllOpenContracts();
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE contract SET \"status\" = 'Closed' WHERE \"oid\" = ?1", nativeQuery = true)
	void closeContract(Long contractId);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE contract SET \"technical_auth\" = ?2 WHERE \"id\" = ?1", nativeQuery = true)
	void setTechAuth(Integer contractId, Long peopleId);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE contract SET \"contracting_auth\" = ?2 WHERE \"id\" = ?1", nativeQuery = true)
	void setContractingAuth(Integer contractId, Long peopleId);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE contract SET \"vendor_auth\" = ?2 WHERE \"id\" = ?1", nativeQuery = true)
	void setVendorAuth(Integer contractId, Long peopleId);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE contract SET \"service_provider\" = ?2 WHERE \"id\" = ?1", nativeQuery = true)
	void setServiceProvider(Integer contractId, String name);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE contract SET \"client\" = ?2 WHERE \"id\" = ?1", nativeQuery = true)
	void setClient(Integer contractId, String name);
	
}
