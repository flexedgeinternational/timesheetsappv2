package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.AppUser;
import com.example.model.AppUserRole;
import com.example.model.Role;

public interface AppUserRoleRepository extends JpaRepository<AppUserRole, Integer> {

	List<AppUserRole> findByAppUser(AppUser appuser);

	List<AppUserRole> findByAppUserAndRole(AppUser appuser, Role role);

}
