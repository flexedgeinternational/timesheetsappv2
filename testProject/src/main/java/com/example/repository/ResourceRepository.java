package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.example.model.AppUser;

public interface ResourceRepository extends JpaRepository<AppUser, Integer>, JpaSpecificationExecutor<AppUser> {

}
