package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.model.MenuItem;

@Repository
public interface MenuItemRepository extends JpaRepository<MenuItem, Integer> {
	List<MenuItem> findByParentMenuItem(MenuItem menuitem);

	MenuItem findByMenuTitle(String menuTitle);

	@Query(value = "select mi.oid, mi.menu_title, mi.menu_view_name, mi.fa_icon_name from menu_item mi inner join menu_item_role mir on mi.oid = mir.oid_menu_item inner join app_user_role r on mir.oid_role = r.oid_role where mi.oid_parent_menu_item = ?2 and r.oid_app_user = ?1 order by mi.sort_order", nativeQuery = true)
	List<Object[]> findByAppUserAndParent(Integer oidAppUser, Integer oidParentMenu);

	@Query(value = "select mi.oid, mi.menu_title, mi.menu_view_name, mi.fa_icon_name from menu_item mi inner join menu_item_role mir on mi.oid = mir.oid_menu_item inner join app_user_role r on mir.oid_role = r.oid_role where mi.oid_parent_menu_item is null and r.oid_app_user = ?1 order by mi.sort_order", nativeQuery = true)
	List<Object[]> findRootByAppUser(Integer oidAppUser);

	@Query(value = "select m.oid,r.name from menu_item m INNER join menu_item_role mir on m.oid=mir.oid_menu_item INNER join role r on mir.oid_role=r.oid where m.oid=?1", nativeQuery = true)
	List<Object[]> findRolesByMenuItem(Integer oidMenuItem);

	@Query(value = "select * from menu_item m INNER join menu_item_role mir on m.oid=mir.oid_menu_item INNER join role r on mir.oid_role=r.oid where r.name=?1", nativeQuery = true)
	List<MenuItem> findByRole(String roleName);

	@Query(value = "select * from menu_item m INNER join menu_item_role mir on m.oid=mir.oid_menu_item INNER join role r on mir.oid_role=r.oid where m.oid NOT IN ?1", nativeQuery = true)
	List<MenuItem> findMenuItemByRoleAndNotInList(List<Integer> menuItems);

}
