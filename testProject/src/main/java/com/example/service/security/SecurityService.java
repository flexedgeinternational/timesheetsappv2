package com.example.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.example.model.AppUser;
import com.example.repository.AppUserRepository;

@Service
@Configurable
public class SecurityService {

	@Autowired
	AppUserRepository appUserRepo;

	public AppUser retrieveAppUserByUsername(String username) {
		return appUserRepo.findByUsername(username);
	}
	
	public String getRole(String username) {
		return appUserRepo.getUserRole(username); 
	}

}
