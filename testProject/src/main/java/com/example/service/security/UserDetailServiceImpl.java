package com.example.service.security;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.model.AppUser;
import com.example.repository.AppUserRepository;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

	protected static final Logger log = LoggerFactory.getLogger(UserDetailServiceImpl.class);

	@Autowired
	private AppUserRepository appUserRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AppUser appUser = appUserRepo.findByUsername(username);
		if ((appUser == null)) {
			return null;
		}
		log.info(appUser.getUsername() + " logged in sucessfully!");
		return new org.springframework.security.core.userdetails.User(appUser.getUsername(), appUser.getPassword(),
				appUser.getActive(), true, true, true, new ArrayList<GrantedAuthority>());
	}
}