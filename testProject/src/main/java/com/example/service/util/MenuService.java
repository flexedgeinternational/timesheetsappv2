package com.example.service.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.example.bean.MenuItemBean;
import com.example.localization.Localizer;
import com.example.model.AppUser;
import com.example.model.MenuItem;
import com.example.model.MenuItemRole;
import com.example.repository.AppUserRepository;
import com.example.repository.AppUserRoleRepository;
import com.example.repository.MenuItemRepository;
import com.example.repository.MenuItemRoleRepository;
import com.example.repository.RoleRepository;
import com.example.service.crud.EntityService;

@Service
@Configurable
public class MenuService implements EntityService<MenuItem> {

	@Autowired
	MenuItemRepository menuItemRepository;

	@Autowired
	MenuItemRoleRepository menuItemRoleRepository;

	@Autowired
	AppUserRoleRepository appUserRoleRepository;

	@Autowired
	AppUserRepository appUserRepository;

	@Autowired
	RoleRepository roleRepository;

	public List<MenuItemBean> retrieveMenuItems(AppUser user, MenuItemBean parentMenu) {
		List<MenuItemBean> menuItemBeans = new ArrayList<>();
		if (user != null) {
			List<Object[]> results;
			if (parentMenu == null) {
				results = menuItemRepository.findRootByAppUser(user.getOid());
			} else {
				results = menuItemRepository.findByAppUserAndParent(user.getOid(), parentMenu.getOid());
			}
			for (Object[] result : results) {
				Integer oid = (Integer) result[0];
				String title = (String) result[1];
				String viewName = (String) result[2];
				String faIconName = (String) result[3];
				MenuItemBean menuItemBean = new MenuItemBean(oid, parentMenu, title, viewName, faIconName);
				List<MenuItemBean> children = retrieveMenuItems(user, menuItemBean);
				menuItemBean.setChildren(children);
				menuItemBeans.add(menuItemBean);
			}
		}
		return menuItemBeans;
	}

	@Override
	public Class<MenuItem> getEntityClass() {
		return MenuItem.class;
	}

	@Override
	public String getListViewName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getEditViewTitle(Object object) {
		return Localizer.getLocalizedTerm("Menu Item Add / Edit");
	}

	@Override
	public List<MenuItem> getListObjects() {
		return menuItemRepository.findAll();
	}

	@Override
	public Long getSelectedRowId(Object selectedRow) {
		return new Long(((MenuItem) selectedRow).getOid());
	}

	@Override
	public MenuItem loadObject(Long objectId) {
		return menuItemRepository.findOne(objectId.intValue());
	}

	@Override
	public MenuItem createObject() {
		return new MenuItem();
	}

	@Override
	public void saveObject(MenuItem object) {
		menuItemRepository.save(object);
	}

	@Override
	public void deleteSelectedRows(Collection<Object> selectedRows) {
		for (Object row : selectedRows) {
			menuItemRepository.delete((MenuItem) row);
		}
	}

	public void deleteOneSelectedRow(MenuItem selectedRow) {
		menuItemRepository.delete(selectedRow);

	}

	public MenuItem getMenuItemByTitle(String menuTitle) {
		return menuItemRepository.findByMenuTitle(menuTitle);
	}

	public List<MenuItem> findByParentMenuItem(MenuItem menuItem) {
		return menuItemRepository.findByParentMenuItem(menuItem);
	}

	public Object[] getSpecificMenuItem(List<MenuItem> menuItems) {
		List<Integer> alreadyExist = new ArrayList<>();
		List<Object> finalList = new ArrayList<Object>(new ArrayList<>());
		// justForTest
		finalList.add("");
		for (MenuItem menuItem : menuItems) {

			if (findByParentMenuItem(menuItem).isEmpty()) {
				if (!alreadyExist.contains(menuItem.getOid())) {
					finalList.add(menuItem/* .getMenuTitle() */);
				}
			} else {
				for (MenuItem child : findByParentMenuItem(menuItem)) {
					finalList.add(new Object[] { menuItem/* .getMenuTitle() */,
							child/* .getMenuTitle() */ });
					alreadyExist.add(child.getOid());
				}
			}
		}
		Object[] treeMenuItems = finalList.toArray(new Object[finalList.size()]);
		return treeMenuItems;
	}

	public List<Object[]> findRolesByMenuItem(MenuItem currentObject) {
		return menuItemRepository.findRolesByMenuItem(currentObject.getOid());
	}

	public void saveMenuItemRole(MenuItemRole menuItemRole) {
		menuItemRoleRepository.save(menuItemRole);
	}

	public List<MenuItem> findByRole(String roleName) {
		return menuItemRepository.findByRole(roleName);
	}

	public List<MenuItem> findMenuItemByRoleAndNotInList(List<MenuItem> menuItems) {

		List<Integer> listOid = new ArrayList<>();
		for (MenuItem menuItem : menuItems) {
			listOid.add(menuItem.getOid());
		}
		if (!listOid.isEmpty()) {
			return menuItemRepository.findMenuItemByRoleAndNotInList(listOid);

		} else {
			return getListObjects();
		}
	}

	public MenuItem findByTitle(String menuTitle) {
		return menuItemRepository.findByMenuTitle(menuTitle);

	}

	public void deleteSelectedRowsFromCurrentMenuItems(Collection<Object> selectedRows, Integer oidRole) {
		for (Object row : selectedRows) {
			menuItemRoleRepository.deleteByMenuAndRole(((MenuItem) row).getOid(), oidRole);
		}

	}
}
