package com.example.service.crud;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.example.localization.Localizer;
import com.example.model.Employee;
import com.example.repository.EmployeeRepository;

@Service
@Configurable
public class EmployeeService implements EntityService<Employee> {

	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public Class<Employee> getEntityClass() {
		return Employee.class;
	}

	@Override
	public List<Employee> getListObjects() {
		return employeeRepository.findAll();
	}

	@Override
	public Long getSelectedRowId(Object selectedRow) {
		return ((Employee) selectedRow).getOid();
	}

	@Override
	public void deleteSelectedRows(Collection<Object> selectedRows) {
		for (Object row : selectedRows) {
			employeeRepository.delete((Employee) row);
		}
	}

	@Override
	public void saveObject(Employee object) {
		employeeRepository.save(object);
	}

	@Override
	public Employee createObject() {
		return new Employee();
	}

	@Override
	public Employee loadObject(Long objectId) {
		return employeeRepository.findOne(objectId);
	}

	@Override
	public String getListViewName() {
		return this.getEntityClass().getSimpleName() + "ListView";
	}

	@Override
	public String getEditViewTitle(Object object) {
		return Localizer.getLocalizedTerm("Employee Add / Edit");
	}

}
