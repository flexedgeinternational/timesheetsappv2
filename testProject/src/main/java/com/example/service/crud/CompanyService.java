package com.example.service.crud;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.example.localization.Localizer;
import com.example.model.Company;
import com.example.model.Person;
import com.example.repository.CompanyRepository;

@Service
@Configurable
public class CompanyService implements EntityService<Company> {

	@Autowired
	CompanyRepository companyRepository;
	
	@Override
	public Class<Company> getEntityClass() {
		return Company.class;
	}

	@Override
	public String getListViewName() {
		return this.getEntityClass().getSimpleName() + "ListView";
	}

	@Override
	public List<Company> getListObjects() {
		return companyRepository.findAll();
	}


	@Override
	public Company createObject() {
		return new Company();
	}

	@Override
	public void saveObject(Company object) {
		companyRepository.save(object);
	}

	@Override
	public void deleteSelectedRows(Collection<Object> selectedRows) {
		for (Object row : selectedRows) {
			companyRepository.delete((Company) row);
		}
	}

	@Override
	public String getEditViewTitle(Object object) {
		return Localizer.getLocalizedTerm("client");
	}

	@Override
	public Long getSelectedRowId(Object selectedRow) {
		return ((Company) selectedRow).getOid();
	}

	@Override
	public Company loadObject(Long objectId) {
		return companyRepository.findOne(objectId);
	}

	public List<Object[]> getCompanies() {
		return companyRepository.getCompanies();
	}
	
}
