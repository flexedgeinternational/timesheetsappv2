package com.example.service.crud;

import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.example.localization.Localizer;
import com.example.model.AppUser;
import com.example.model.Contract;
import com.example.model.Employee;
import com.example.repository.ContractRepository;

@Service
@Configurable
public class ContractService implements EntityService<Contract> {

	@Autowired
	ContractRepository contractRepository;
	
	@Override
	public Class<Contract> getEntityClass() {
		return Contract.class;
	}

	@Override
	public String getListViewName() {
		return this.getEntityClass().getSimpleName() + "ListView";
	}

	@Override
	public String getEditViewTitle(Object object) {
		return Localizer.getLocalizedTerm("Contract.add.edit");
	}

	@Override
	public List<Contract> getListObjects() {
		return contractRepository.findAll();
	}
	
	public List<Contract> getListObjects(AppUser appUser) {
		
		System.out.println("appUser = " + appUser.getUsername());
		
		return contractRepository.getClosedContracts(appUser.getUsername());
	}
	
	@Override
	public Long getSelectedRowId(Object selectedRow) {
		return ((Contract) selectedRow).getId();
	}

	@Override
	public Contract loadObject(Long objectId) {
		//return contractRepository.findOne(Long.decode(objectId.toString()));
		return contractRepository.findOne(objectId);
	}

	@Override
	public Contract createObject() {
		return new Contract();
	}

	@Override
	public void saveObject(Contract object) {
		contractRepository.save(object);
	}

	@Override
	public void deleteSelectedRows(Collection<Object> selectedRows) {
		for (Object row : selectedRows) {
			contractRepository.delete((Contract) row);
		}
	}

	public void closeContract(Long contractId) {
		contractRepository.closeContract(contractId);
	}
	
	public boolean hasPhases(int contractId) {
		
		List<Object[]> timesheets = contractRepository.findPhasesByContractId(contractId);
		
		if(timesheets.isEmpty()) 
			return false;
		
		return false;
	}
	
	public boolean hasTasks(int contractId) {
		
		List<Object[]> timesheets = contractRepository.findTasksByContractId(contractId);
		
		if(timesheets.isEmpty()) 
			return false;
		
		return false;
	}
	
	
	public List<Object[]> getResourceContracts(String resourceName) {
		return contractRepository.getResourceContracts(resourceName);
	}
	
	public String getClient(int contractId) {
		return contractRepository.getClient(contractId);
	}
	
	public String getServiceProvider(int contractId) {
		return contractRepository.getServiceProvider(contractId);
	}
	
	public Long getTechAuth(int contractId) {
		return contractRepository.getTechAuth(contractId);
	}
	
	public Long getContractingAuth(int contractId) {
		return contractRepository.getContractingAuth(contractId);
	}
	
	public Long getVendorAuth(int contractId) {
		return contractRepository.getVendorAuth(contractId);
	}
	
	public List<Object[]> getPeople() {
		return contractRepository.getPeople();
	}
	
	public List<String> getCompanies() {
		return contractRepository.getCompanies();
	}
	
	public List<String> getCurrencies() {
		return contractRepository.getCurrencies();
	}
	
	public int getTotalCost(Long contractId) {
		return contractRepository.getTotalCost(contractId);
	}
	
	public int getTotalDailyCost(int contractId) {
		return contractRepository.getTotalDailyCost(contractId);
	}
	
	public void setAuth(int contractId, long peopleId, String type) {
		if (type.equals("ContractingAuthority")) {
			contractRepository.setContractingAuth(contractId, peopleId);
		} else if (type.equals("VendorAuthority")) {
			contractRepository.setVendorAuth(contractId, peopleId);
		} else if (type.equals("TechnicalAuthority")) {
			contractRepository.setTechAuth(contractId, peopleId);
		}
	}
	
	public void setCompany(int contractId, String name, String type) {
		if (type.equals("ServiceProvider")) {
			contractRepository.setServiceProvider(contractId, name);
		} else if (type.equals("Client")) {
			contractRepository.setClient(contractId, name);
		}
	}
	
	public int totalMonths(Long contractId) {
		return 12;
	}
	
}
