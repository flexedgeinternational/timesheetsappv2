package com.example.service.crud;

import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.example.localization.Localizer;
import com.example.model.File;
import com.example.repository.FileRepository;

@Service
@Configurable
public class FileService implements EntityService<File>  { 
		
	@Autowired
	FileRepository fileRepository;
	
	@Override
	public Class<File> getEntityClass() {
		return File.class;
	}
	
	@Override
	public String getListViewName() {
		return this.getEntityClass().getSimpleName() + "ListView";
	}
	
	@Override
	public String getEditViewTitle(Object object) {
		return Localizer.getLocalizedTerm("edit.template");
	}
	
	@Override
	public List<File> getListObjects() {
		return fileRepository.findAll();
	}
	
	@Override
	public Long getSelectedRowId(Object selectedRow) {
		return ((File) selectedRow).getOid();
	}
	
	@Override
	public File loadObject(Long objectId) {
		return fileRepository.findOne(objectId);
	}
	
	@Override
	public File createObject() {
		return new File();
	}
	
	@Override
	public void saveObject(File object) {
		fileRepository.save(object);
	}
	
	@Override
	public void deleteSelectedRows(Collection<Object> selectedRows) {
		for (Object row : selectedRows) {
			fileRepository.delete((File) row);
		}
	}
}
