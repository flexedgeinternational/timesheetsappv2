package com.example.service.crud;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.example.localization.Localizer;
import com.example.model.Role;
import com.example.repository.RoleRepository;

@Service
@Configurable
public class RoleService implements EntityService<Role> {

	@Autowired
	RoleRepository roleRepository;

	@Override
	public Class<Role> getEntityClass() {
		return Role.class;
	}

	@Override
	public List<Role> getListObjects() {
		return roleRepository.findAll();
	}

	@Override
	public Long getSelectedRowId(Object selectedRow) {
		return new Long(((Role) selectedRow).getOid());
	}

	@Override
	public void deleteSelectedRows(Collection<Object> selectedRows) {
		for (Object row : selectedRows) {
			roleRepository.delete((Role) row);
		}
	}

	@Override
	public void saveObject(Role object) {
		roleRepository.save(object);
	}

	@Override
	public Role createObject() {
		return new Role();
	}

	@Override
	public Role loadObject(Long objectId) {
		return roleRepository.findOne(objectId.intValue());
	}

	@Override
	public String getListViewName() {
		return this.getEntityClass().getSimpleName() + "ListView";
	}

	@Override
	public String getEditViewTitle(Object object) {
		return Localizer.getLocalizedTerm("Role Add / Edit");
	}

	public List<Role> findAllByName(String caption) {
		return roleRepository.findByName(caption);
	}

}
