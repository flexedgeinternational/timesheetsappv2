package com.example.service.crud;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.example.localization.Localizer;
import com.example.model.Timesheet;
import com.example.repository.TimesheetRepository;

@Service
@Configurable
public class TimesheetService implements EntityService<Timesheet> {

	@Autowired
	TimesheetRepository timesheetRepository;
	
	@Override
	public Class<Timesheet> getEntityClass() {
		return Timesheet.class;
	}

	@Override
	public String getListViewName() {
		return this.getEntityClass().getSimpleName() + "ListView";
	}

	@Override
	public String getEditViewTitle(Object object) {
		return Localizer.getLocalizedTerm("timesheet.template");
	}

	@Override
	public List<Timesheet> getListObjects() {
		return timesheetRepository.findAll();
	}

	public List<Timesheet> getListObjects(int contractId, String username) {
		return timesheetRepository.findAll();
	}
	
	@Override
	public Long getSelectedRowId(Object selectedRow) {
		return ((Timesheet) selectedRow).getOid();
	}

	@Override
	public Timesheet loadObject(Long objectId) {
		return timesheetRepository.findOne(objectId);
	}

	@Override
	public Timesheet createObject() {
		return new Timesheet();
	}

	@Override
	public void saveObject(Timesheet object) {
		
		System.out.println("object.getOid = " + object.getOid());
		
		if (object.getOid() == 0) {
			System.out.println("Saving!");
			timesheetRepository.customSave(
				object.getStart_date(),
				object.getEnd_date(),
				object.getMon_reg(),
				object.getTues_reg(),
				object.getWed_reg(),
				object.getThurs_reg(),
				object.getFri_reg(),
				object.getSat_reg(),
				object.getSun_reg(),
				object.getMon_ot(),
				object.getTues_ot(),
				object.getWed_ot(),
				object.getThurs_ot(),
				object.getFri_ot(),
				object.getSat_ot(),
				object.getSun_ot(),
				object.getContract_resource_oid().getOid(),
				object.getTimesheet_oid().getOid(),
				object.getSubmitted()
			);
		} else {
			System.out.println("Updating!");
			timesheetRepository.customUpdate(
				object.getStart_date(),
				object.getEnd_date(),
				object.getMon_reg(),
				object.getTues_reg(),
				object.getWed_reg(),
				object.getThurs_reg(),
				object.getFri_reg(),
				object.getSat_reg(),
				object.getSun_reg(),
				object.getMon_ot(),
				object.getTues_ot(),
				object.getWed_ot(),
				object.getThurs_ot(),
				object.getFri_ot(),
				object.getSat_ot(),
				object.getSun_ot(),
				object.getContract_resource_oid().getOid(),
				object.getTimesheet_oid().getOid(),
				object.getSubmitted(),
				object.getOid()
			);
		}
	}

	@Override
	public void deleteSelectedRows(Collection<Object> selectedRows) {
		for (Object row : selectedRows) {
			
			Timesheet timesheet = (Timesheet)row;
			timesheetRepository.customDelete(timesheet.getOid());
		}
	}	
	
}
