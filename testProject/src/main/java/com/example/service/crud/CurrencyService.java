package com.example.service.crud;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.example.localization.Localizer;
import com.example.model.Currency;
import com.example.repository.CurrencyRepository;

@Service
@Configurable
public class CurrencyService implements EntityService<Currency>  {

	@Autowired
	CurrencyRepository currencyRepository;
	
	@Override
	public Class<Currency> getEntityClass() {
		return Currency.class;
	}

	@Override
	public String getListViewName() {
		return this.getEntityClass().getSimpleName() + "ListView";
	}

	@Override
	public String getEditViewTitle(Object object) {
		return Localizer.getLocalizedTerm("");
	}

	@Override
	public List<Currency> getListObjects() {
		return currencyRepository.findAll();
	}

	@Override
	public Long getSelectedRowId(Object selectedRow) {
		return null;
	}

	@Override
	public Currency loadObject(Long objectId) {
		return null;
	}

	@Override
	public Currency createObject() {
		return new Currency();
	}

	@Override
	public void saveObject(Currency object) {
		currencyRepository.save(object);
	}

	@Override
	public void deleteSelectedRows(Collection<Object> selectedRows) {
		for (Object row : selectedRows) {
			currencyRepository.delete((Currency) row);
		}
	}

	public List<String> getCurrencies() {
		return currencyRepository.getCurrencies();
	}
	
}
