package com.example.service.crud;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.example.bean.PasswordChangeBean;
import com.example.localization.Localizer;
import com.example.model.AppUser;
import com.example.repository.AppUserRepository;

@Service
@Configurable
public class AppUserService implements EntityService<AppUser> {

	@Autowired
	AppUserRepository appUserRepository;

	@Override
	public Class<AppUser> getEntityClass() {
		return AppUser.class;
	}

	@Override
	public List<AppUser> getListObjects() {
		return appUserRepository.findAll();
	}

	@Override
	public Long getSelectedRowId(Object selectedRow) {
		return new Long(((AppUser) selectedRow).getOid());
	}

	@Override
	public void deleteSelectedRows(Collection<Object> selectedRows) {
		for (Object row : selectedRows) {
			appUserRepository.delete((AppUser) row);
		}
	}

	@Override
	public void saveObject(AppUser object) {
		appUserRepository.save(object);
	}

	@Override
	public AppUser createObject() {
		return new AppUser();
	}

	@Override
	public AppUser loadObject(Long objectId) {
		return appUserRepository.findOne(objectId.intValue());
	}

	@Override
	public String getListViewName() {
		return this.getEntityClass().getSimpleName() + "ListView";
	}

	@Override
	public String getEditViewTitle(Object object) {
		return Localizer.getLocalizedTerm("AppUser Add / Edit");
	}

	public List<Object[]> findRolesByAppUser(AppUser appUser) {
		return appUserRepository.findRolesByAppUser(appUser.getOid());
	}

	public void saveChangedPassword(PasswordChangeBean bean) {
		AppUser appUser = bean.getAppUser();
		appUser.setPassword(bean.getPassword());
		saveObject(appUser);

	}
}
