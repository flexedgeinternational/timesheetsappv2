package com.example.service.crud;

import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import com.example.localization.Localizer;
import com.example.model.ContractResource;
import com.example.repository.ContractResourceRepository;

@Service
@Configurable
public class ContractResourceService implements EntityService<ContractResource> {

	@Autowired
	ContractResourceRepository ContractResourceRepository;
	
	@Override
	public Class<ContractResource> getEntityClass() {
		return ContractResource.class;
	}

	@Override
	public String getListViewName() {
		return this.getEntityClass().getSimpleName() + "ListView";
	}

	@Override
	public String getEditViewTitle(Object object) {
		return Localizer.getLocalizedTerm("ContractResource.template");
	}

	@Override
	public List<ContractResource> getListObjects() {
		return ContractResourceRepository.findAll();
	}

	@Override
	public Long getSelectedRowId(Object selectedRow) {
		return ((ContractResource) selectedRow).getOid();
	}

	@Override
	public ContractResource loadObject(Long objectId) {
		return ContractResourceRepository.findOne(objectId);
	}

	@Override
	public ContractResource createObject() {
		return new ContractResource();
	}

	@Override
	public void saveObject(ContractResource object) {
		ContractResourceRepository.save(object);
	}

	@Override
	public void deleteSelectedRows(Collection<Object> selectedRows) {
		for (Object row : selectedRows) {
			ContractResourceRepository.delete((ContractResource) row);
		}
	}	
	
	public ContractResource getContractResource(int contractId, String username) {
		return ContractResourceRepository.getContractResource(contractId, username);
	}
	
	public ContractResource getContractResource(long contractId, String username) {
		
		Integer intContractId = (int) (long) contractId;
		
		return ContractResourceRepository.getContractResource(intContractId, username);
	}
	
}
