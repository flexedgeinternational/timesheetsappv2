package com.example.service.crud;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.example.localization.Localizer;
import com.example.model.Company;
import com.example.model.Contract;
import com.example.model.Person;
import com.example.repository.PersonRepository;

@Service
@Configurable
public class PersonService implements EntityService<Person> {

	@Autowired
	PersonRepository personRepository;

	@Override
	public Class<Person> getEntityClass() {
		return Person.class;
	}

	@Override
	public String getListViewName() {
		return this.getEntityClass().getSimpleName() + "ListView";
	}
	
	@Override
	public List<Person> getListObjects() {
		return personRepository.findAll();
	}

	@Override
	public Long getSelectedRowId(Object selectedRow) {
		return ((Person) selectedRow).getOid();
	}

	@Override
	public Person createObject() {
		return new Person();
	}

	@Override
	public void saveObject(Person object) {
		personRepository.save(object);	
	}
	
	@Override
	public void deleteSelectedRows(Collection<Object> selectedRows) {
		for (Object row : selectedRows) {
			personRepository.delete((Person) row);
		}
	}

	@Override
	public String getEditViewTitle(Object object) {
		return Localizer.getLocalizedTerm("people");
	}

	@Override
	public Person loadObject(Long objectId) {
		return personRepository.findOne(objectId);
	}

	public List<Object[]> getPeople() {
		return personRepository.getPeople();
	}
}
