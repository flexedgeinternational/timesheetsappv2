package com.example.service.crud;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

@Service
@Configurable 
public class ResourceTimesheetService extends TimesheetService {

	@Override
	public String getListViewName() {
		return "Resource" + this.getEntityClass().getSimpleName() + "ListView";
	}
	
}
