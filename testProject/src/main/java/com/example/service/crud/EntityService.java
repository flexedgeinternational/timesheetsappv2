package com.example.service.crud;

import java.util.Collection;
import java.util.List;

public interface EntityService<T> {

	public Class<T> getEntityClass();

	public String getListViewName();

	public String getEditViewTitle(Object object);

	public List<T> getListObjects();

	public Long getSelectedRowId(Object selectedRow);

	public T loadObject(Long objectId);

	public T createObject();

	public void saveObject(T object);

	public void deleteSelectedRows(Collection<Object> selectedRows);

}