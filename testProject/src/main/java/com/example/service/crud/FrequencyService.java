package com.example.service.crud;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.example.localization.Localizer;
import com.example.model.Contract;
import com.example.model.Frequency;
import com.example.repository.FrequencyRepository;

@Service
@Configurable
public class FrequencyService implements EntityService<Frequency> {

	@Autowired
	FrequencyRepository frequencyRepository;

	@Override
	public Class<Frequency> getEntityClass() {
		return Frequency.class;
	}

	@Override
	public String getListViewName() {
		return this.getEntityClass().getSimpleName() + "ListView";
	}
	
	@Override
	public List<Frequency> getListObjects() {
		return frequencyRepository.findAll();
	}

	@Override
	public Long getSelectedRowId(Object selectedRow) {
		return ((Frequency) selectedRow).getOid();
	}

	@Override
	public Frequency createObject() {
		return new Frequency();
	}

	@Override
	public void saveObject(Frequency object) {
		frequencyRepository.save(object);	
	}
	
	@Override
	public void deleteSelectedRows(Collection<Object> selectedRows) {
		for (Object row : selectedRows) {
			frequencyRepository.delete((Frequency) row);
		}
	}

	@Override
	public String getEditViewTitle(Object object) {
		return "frequency";
		//return Localizer.getLocalizedTerm("");
	}

	@Override
	public Frequency loadObject(Long objectId) {
		return frequencyRepository.findOne(objectId);
	}

	public List<Object[]> getFrequencies() {
		return frequencyRepository.getFrequencies();
	}
	
}
