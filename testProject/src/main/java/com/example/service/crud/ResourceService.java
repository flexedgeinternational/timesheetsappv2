package com.example.service.crud;

import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.example.localization.Localizer;
import com.example.model.AppUser;

@Service
@Configurable 
public class ResourceService extends AppUserService implements EntityService<AppUser> {

	@Override
	public String getEditViewTitle(Object object) {
		return Localizer.getLocalizedTerm("resource.edit");
	}
	
	@Override
	public List<AppUser> getListObjects() {
		return appUserRepository.findAllResources();
	}
	
}
