package com.example.service.crud;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.example.localization.Localizer;
import com.example.model.TimesheetTemplate;
import com.example.repository.TimesheetTemplateRepository;

@Service
@Configurable
public class TimesheetTemplateService implements EntityService<TimesheetTemplate> {

	@Autowired
	TimesheetTemplateRepository timesheetTemplateRepository;
	
	@Override
	public Class<TimesheetTemplate> getEntityClass() {
		return TimesheetTemplate.class;
	}

	@Override
	public String getListViewName() {
		return this.getEntityClass().getSimpleName() + "ListView";
	}

	@Override
	public String getEditViewTitle(Object object) {
		return Localizer.getLocalizedTerm("timesheet.template");
	}

	@Override
	public List<TimesheetTemplate> getListObjects() {
		return timesheetTemplateRepository.findAll();
	}

	@Override
	public Long getSelectedRowId(Object selectedRow) {
		return ((TimesheetTemplate) selectedRow).getOid();
	}

	@Override
	public TimesheetTemplate loadObject(Long objectId) {
		return timesheetTemplateRepository.findOne(objectId);
	}

	@Override
	public TimesheetTemplate createObject() {
		return new TimesheetTemplate();
	}

	@Override
	public void saveObject(TimesheetTemplate object) {
		timesheetTemplateRepository.save(object);
	}

	@Override
	public void deleteSelectedRows(Collection<Object> selectedRows) {
		for (Object row : selectedRows) {
			timesheetTemplateRepository.delete((TimesheetTemplate) row);
		}
	}

	public void setAsDefault(Long templateId) {
		timesheetTemplateRepository.setAsDefault(templateId);
	}
	
	public TimesheetTemplate getDefaultTemplate() {
		return timesheetTemplateRepository.getDefaultTemplate();
	}
	
}
