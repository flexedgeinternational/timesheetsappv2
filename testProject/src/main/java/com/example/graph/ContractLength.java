package com.example.graph;

import com.example.util.DateValuePair;
import com.example.util.GetResource;
import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.ChartEnum;
import com.vaadin.addon.charts.model.ChartType;
import com.vaadin.addon.charts.model.Configuration;
import com.vaadin.addon.charts.model.DataSeries;
import com.vaadin.addon.charts.model.DataSeriesItem;

@SuppressWarnings("serial")
public class ContractLength extends Chart {

	public ChartEnum graph;
	public int totalMonths;
	double totalVal;
	
	public ContractLength(Long id, int val, double totalVal, int numMonths) {
		this.totalVal = totalVal;
		
		Configuration conf = this.getConfiguration();
		conf.setTitle("Contract Length Given Constant Expenditure");
		conf.getChart().setType(ChartType.COLUMN);
		
		DataSeries data = new DataSeries("Remaining Cost");
		totalMonths = 0;
		for (DateValuePair pair : GetResource.getDateArrayListFromVal(id, val, totalVal)) {
			data.add(new DataSeriesItem(pair.getDate(), pair.getValue()));
			totalMonths += 1;
		}
		
		conf.addSeries(data);
		conf.getxAxis().setTitle("Month");
		conf.getxAxis().setCategories();
		conf.getyAxis().setTitle("Expenditure");
		
		this.setConfiguration(conf);
		
	}
	
	public void Update(Long id, int val) {
		
		Configuration conf = this.getConfiguration();
		
		DataSeries data = new DataSeries("Remaining Cost");
		totalMonths = 0;
		for (DateValuePair pair : GetResource.getDateArrayListFromVal(id, val, totalVal)) {
			data.add(new DataSeriesItem(pair.getDate(), pair.getValue()));
			//lastMonth = pair.getDate();
			totalMonths += 1;
		}
		
		conf.setSeries(data);
		this.setConfiguration(conf);
		this.drawChart();
		
	}

}
