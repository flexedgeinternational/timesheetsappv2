package com.example.graph;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.example.util.GetResource;
import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.ChartType;
import com.vaadin.addon.charts.model.Configuration;
import com.vaadin.addon.charts.model.DataSeries;
import com.vaadin.addon.charts.model.ListSeries;
import com.vaadin.addon.charts.model.PlotOptionsColumn;
import com.vaadin.addon.charts.model.Series;
import com.vaadin.addon.charts.model.StackLabels;
import com.vaadin.addon.charts.model.Stacking;
import com.vaadin.addon.charts.model.Tooltip;
import com.vaadin.addon.charts.model.XAxis;
import com.vaadin.addon.charts.model.YAxis;

@SuppressWarnings("serial")
public class MonthlyCompounding extends Chart {

	private Configuration conf;
	
	//private Map<Integer, String> months;

	private int currentMonth;
	private int monthsToApril;
	private int currentYear;
	private int totalSpent;
	private int dailyCost;
	
	private Calendar cal;
	
	private ListSeries monthlyCostListSeries;
	private ListSeries totalCostListSeries;
		
	/* Refactor this to make it more readable */
	public MonthlyCompounding(int dailyCost, Calendar cal) {
		
		super(ChartType.COLUMN);
		
		this.cal = cal;
		this.dailyCost = dailyCost;
		 
		conf = this.getConfiguration();
		conf.setTitle("Total Spent - By Month");
		conf.getChart().setType(ChartType.COLUMN);
		
		currentMonth = cal.get(Calendar.MONTH);
		currentYear = cal.get(Calendar.YEAR);
		
		setNumberMonthsToApril();
		
		setXAxis();
		totalSpent = 0;
		loadData();
		
		conf.addSeries(monthlyCostListSeries);
		conf.addSeries(totalCostListSeries);
		
		YAxis yAxis = new YAxis();
		StackLabels sLabels = new StackLabels(true);
        yAxis.setStackLabels(sLabels);
        yAxis.setTitle("Expenditure");
        conf.addyAxis(yAxis);
        
        PlotOptionsColumn plotOptions = new PlotOptionsColumn();
        plotOptions.setStacking(Stacking.NORMAL);
        conf.setPlotOptions(plotOptions);
        
        Tooltip tooltip = new Tooltip();
        tooltip.setFormatter("function() { return '<b>'+ this.x +'</b><br/>"
                + "'+this.series.name +': '+ this.y +'<br/>'+'Total: '+ this.point.stackTotal;}");
        conf.setTooltip(tooltip);
        
		this.setConfiguration(conf);
	}
	
	private void setXAxis() {
		XAxis xAxis = new XAxis();
		
		ArrayList<String> arrList = new ArrayList<String>();
		
		for (int i = currentMonth; i < currentMonth + monthsToApril; i++) {
			arrList.add(GetResource.months().get((i%12)+1));
		}
		
        xAxis.setCategories(arrList.toArray(new String[0]));
        conf.addxAxis(xAxis);
		
	}

	private void loadData() {
		
		ArrayList<Number> monthlyCostArrList = new ArrayList<Number>();
		ArrayList<Number> totalCostArrList = new ArrayList<Number>();
		
		for (int i = currentMonth; i < currentMonth + monthsToApril; i++) {
			cal.clear();
			cal.set(Calendar.MONTH, ((i%12)+1));
			cal.set(Calendar.YEAR, currentYear);
			//int daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH); 
			
			int workDays = getNumWorkDays();
			
			int monthlyCost = dailyCost * workDays;
			totalSpent += monthlyCost;
			
			monthlyCostArrList.add(monthlyCost);
			totalCostArrList.add(totalSpent - monthlyCost);
						
		}
		
		monthlyCostListSeries = new ListSeries("Monthly Cost", monthlyCostArrList.toArray(new Number[0]));
		totalCostListSeries = new ListSeries("Sum of preceeding months", totalCostArrList.toArray(new Number[0]));
		
		//conf.addSeries(monthlyCostListSeries);
		//conf.addSeries(totalCostListSeries);
	}

	private int getNumWorkDays() {
		
		int workDays = 0;
		
		int totalDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		for(int co=0; co<totalDays; co++){
			cal.add(Calendar.DATE, 1); 
		    
			if (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
	          && cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
	          && !GetResource.holidays().contains((Integer) cal.get(Calendar.DAY_OF_YEAR))) 
			{
	              ++workDays;
			}
			
		}
		return workDays;
	}
	
	private void setNumberMonthsToApril() {
		
		int April = 4;
		
		if (April == currentMonth) {
			monthsToApril = 12;
		}
		else if (April > currentMonth) {
			monthsToApril =  (April - currentMonth);
		} else {
			monthsToApril =  (12 - (currentMonth - April));
		}
	}
	
	
	public void Update(Calendar cal) {
		
		this.conf = this.getConfiguration();
		
		this.cal = cal;
		
		currentMonth = cal.get(Calendar.MONTH);
		currentYear = cal.get(Calendar.YEAR);
		
		setNumberMonthsToApril();
		
		totalSpent = 0;
		
		//removeListSeries();
		loadData();
		
		this.setConfiguration(conf);
		this.drawChart();
		
		/*
		this.conf = this.getConfiguration();
		
		setNumberMonthsToApril();
		
		conf.removexAxes();
		removeListSeries();
		
		setXAxis();
		totalSpent = 0;
		
		
		this.setConfiguration(conf);
		this.drawChart();
		*/
		
	}
	
	public void removeListSeries()
    {
        List<Series> s = new ArrayList<Series>();
        // Use another list - the configuration sends an unmodifiable list, so initialize with a copy of it
        for (Series i : conf.getSeries())
            s.add(i);
        
        for (int i=0;i<s.size();i++)
        {
        	//ListSeries ls = (ListSeries) this.getConfiguration().getSeries().get(i);
            
        	System.out.println("removing " + s.get(i).getName());
            s.remove(i);
            // Update the chart with the new list
            //chart.getConfiguration().setSeries(s);
            //chart.drawChart();
        
        }
    }
	
}
