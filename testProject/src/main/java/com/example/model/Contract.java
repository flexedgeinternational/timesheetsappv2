package com.example.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
public class Contract implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long oid;
	
	@NotNull
	String contractTitle;
	String contractIdentifier;
	double totalCost;
	String currency;
	
	@NotNull
	int client;
	
	@NotNull
	int serviceProvider;
	
	int contractingAuth;
	int technicalAuth;
	int vendorAuth;
	
	Date startDate;
	Date endDate;
	
	@NotNull
	String status;
	
	public long getId() {
		return oid;
	}
	public void setId(long oid) {
		this.oid = oid;
	}
	public String getContractTitle() {
		return contractTitle;
	}
	public void setContractTitle(String contractTitle) {
		this.contractTitle = contractTitle;
	}
	public String getContractIdentifier() {
		return contractIdentifier;
	}
	public void setContractIdentifier(String contractIdentifier) {
		this.contractIdentifier = contractIdentifier;
	}
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public int getClient() {
		return client;
	}
	public void setClient(int client) {
		this.client = client;
	}
	public int getServiceProvider() {
		return serviceProvider;
	}
	public void setServiceProvider(int serviceProvider) {
		this.serviceProvider = serviceProvider;
	}
	public int getContractingAuth() {
		return contractingAuth;
	}
	public void setContractingAuth(int contractingAuth) {
		this.contractingAuth = contractingAuth;
	}
	public int getTechnicalAuth() {
		return technicalAuth;
	}
	public void setTechnicalAuth(int technicalAuth) {
		this.technicalAuth = technicalAuth;
	}
	public int getVendorAuth() {
		return vendorAuth;
	}
	public void setVendorAuth(int vendorAuth) {
		this.vendorAuth = vendorAuth;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
