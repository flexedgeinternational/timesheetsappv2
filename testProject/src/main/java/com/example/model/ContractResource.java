package com.example.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Entity
public class ContractResource implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long oid;
	
	@ManyToOne 
	@JoinColumn(name = "contract_oid")
	Contract contract_oid;
	
	@ManyToOne 
	@JoinColumn(name = "resource_oid")
	AppUser resource_oid;

	public long getOid() {
		return oid;
	}

	public void setOid(long oid) {
		this.oid = oid;
	}

	public Contract getContract_oid() {
		return contract_oid;
	}

	public void setContract_oid(Contract contract_oid) {
		this.contract_oid = contract_oid;
	}

	public AppUser getResource_oid() {
		return resource_oid;
	}

	public void setResource_oid(AppUser resource_oid) {
		this.resource_oid = resource_oid;
	}
	
}
