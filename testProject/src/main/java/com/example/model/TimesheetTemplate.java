package com.example.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
public class TimesheetTemplate implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long oid;
	
	@NotNull
	String name;
	
	@OneToOne //(fetch = FetchType.LAZY)
	@JoinColumn(name = "file")
	File file;

	@ManyToOne
	@JoinColumn(name = "frequency")
	Frequency frequency;
	
	@NotNull
	String file_name;
	
	@NotNull
	int _default;
	
	public long getOid() {
		return oid;
	}

	public void setOid(long oid) {
		this.oid = oid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public int get_default() {
		return _default;
	}

	public void set_default(int _default) {
		this._default = _default;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}
	
	
}
