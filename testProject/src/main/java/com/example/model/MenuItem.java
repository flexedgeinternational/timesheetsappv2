package com.example.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
public class MenuItem implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer oid;

	@NotNull
	private String menuTitle;
	@NotNull
	private Integer sortOrder;
	@NotNull
	private String menuViewName;

	private String faIconName;

	// bi-directional many-to-one association to MenuItem
	@ManyToOne
	@JoinColumn(name = "oid_parent_menu_item")
	private MenuItem parentMenuItem;

	public String getMenuViewName() {
		return menuViewName;
	}

	public void setMenuViewName(String menuViewName) {
		this.menuViewName = menuViewName;
	}

	public MenuItem getParentMenuItem() {
		return parentMenuItem;
	}

	public void setParentMenuItem(MenuItem parentMenuItem) {
		this.parentMenuItem = parentMenuItem;
	}

	@OneToMany(mappedBy = "menuItem", fetch = FetchType.LAZY)
	private List<MenuItemRole> menuItemRoles;

	public Integer getOid() {
		return oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	public String getMenuTitle() {
		return menuTitle;
	}

	public void setMenuTitle(String menuTitle) {
		this.menuTitle = menuTitle;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getFaIconName() {
		return faIconName;
	}

	public void setFaIconName(String faIconName) {
		this.faIconName = faIconName;
	}

	public String toString() {
		return menuTitle;
	}
}
