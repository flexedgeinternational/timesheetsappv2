package com.example.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
@Table(name="TimesheetTotals")
public class Timesheet implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long oid;
	
	@ManyToOne 
	@JoinColumn(name = "contract_resource_oid")
	ContractResource contract_resource_oid;

	@ManyToOne 
	@JoinColumn(name = "timesheet_oid")
	TimesheetTemplate timesheet_oid;
	
	Date start_date;
	Date end_date;
	
	Date submitted;
	
	double mon_reg;
	double tues_reg;
	double wed_reg;
	double thurs_reg;
	double fri_reg;
	double sat_reg;
	double sun_reg;
	
	double mon_ot;
	double tues_ot;
	double wed_ot;
	double thurs_ot;
	double fri_ot;
	double sat_ot;
	double sun_ot;
	
	double reg_sum;
	double ot_sum;
	
	public long getOid() {
		return oid;
	}
	public void setOid(long oid) {
		this.oid = oid;
	}
	public ContractResource getContract_resource_oid() {
		return contract_resource_oid;
	}
	public void setContract_resource_oid(ContractResource contract_resource_oid) {
		this.contract_resource_oid = contract_resource_oid;
	}
	public TimesheetTemplate getTimesheet_oid() {
		return timesheet_oid;
	}
	public void setTimesheet_oid(TimesheetTemplate timesheet_oid) {
		this.timesheet_oid = timesheet_oid;
	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	public Date getEnd_date() {
		return end_date;
	}
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}
	public Date getSubmitted() {
		return submitted;
	}
	public void setSubmitted(Date submitted) {
		this.submitted = submitted;
	}
	public double getMon_reg() {
		return mon_reg;
	}
	public void setMon_reg(double mon_reg) {
		this.mon_reg = mon_reg;
	}
	public double getTues_reg() {
		return tues_reg;
	}
	public void setTues_reg(double tues_reg) {
		this.tues_reg = tues_reg;
	}
	public double getWed_reg() {
		return wed_reg;
	}
	public void setWed_reg(double wed_reg) {
		this.wed_reg = wed_reg;
	}
	public double getThurs_reg() {
		return thurs_reg;
	}
	public void setThurs_reg(double thurs_reg) {
		this.thurs_reg = thurs_reg;
	}
	public double getFri_reg() {
		return fri_reg;
	}
	public void setFri_reg(double fri_reg) {
		this.fri_reg = fri_reg;
	}
	public double getSat_reg() {
		return sat_reg;
	}
	public void setSat_reg(double sat_reg) {
		this.sat_reg = sat_reg;
	}
	public double getSun_reg() {
		return sun_reg;
	}
	public void setSun_reg(double sun_reg) {
		this.sun_reg = sun_reg;
	}
	public double getMon_ot() {
		return mon_ot;
	}
	public void setMon_ot(double mon_ot) {
		this.mon_ot = mon_ot;
	}
	public double getTues_ot() {
		return tues_ot;
	}
	public void setTues_ot(double tues_ot) {
		this.tues_ot = tues_ot;
	}
	public double getWed_ot() {
		return wed_ot;
	}
	public void setWed_ot(double wed_ot) {
		this.wed_ot = wed_ot;
	}
	public double getThurs_ot() {
		return thurs_ot;
	}
	public void setThurs_ot(double thurs_ot) {
		this.thurs_ot = thurs_ot;
	}
	public double getFri_ot() {
		return fri_ot;
	}
	public void setFri_ot(double fri_ot) {
		this.fri_ot = fri_ot;
	}
	public double getSat_ot() {
		return sat_ot;
	}
	public void setSat_ot(double sat_ot) {
		this.sat_ot = sat_ot;
	}
	public double getSun_ot() {
		return sun_ot;
	}
	public void setSun_ot(double sun_ot) {
		this.sun_ot = sun_ot;
	}
	public double getReg_sum() {
		return reg_sum;
	}
	public void setReg_sum(double reg_sum) {
		this.reg_sum = reg_sum;
	}
	public double getOt_sum() {
		return ot_sum;
	}
	public void setOt_sum(double ot_sum) {
		this.ot_sum = ot_sum;
	}
	
}
