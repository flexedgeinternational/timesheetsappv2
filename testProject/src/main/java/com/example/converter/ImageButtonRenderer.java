package com.example.converter;

import java.util.Locale;

import com.example.localization.Localizer;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;

public class ImageButtonRenderer implements Converter<Resource, String> {

	/**
	 * 
	 */

	private Resource resource;
	private static final long serialVersionUID = 1L;

	public ImageButtonRenderer(String status) {
		super();
		if (status.equals(Localizer.getLocalizedTerm("Edit"))) {
			resource = new ThemeResource("img/editButton.png");
		} else if (status.equals(Localizer.getLocalizedTerm("Delete"))) {
			resource = new ThemeResource("img/deleteButton.png");
		}
	}

	@Override
	public String convertToModel(Resource value, Class<? extends String> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		return null;
	}

	@Override
	public Resource convertToPresentation(String value, Class<? extends Resource> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		return resource;

	}

	@Override
	public Class<String> getModelType() {
		return String.class;
	}

	@Override
	public Class<Resource> getPresentationType() {
		return Resource.class;
	}
}
