package com.example.localization;

import java.util.Locale;

import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import com.example.MainUI;
import com.vaadin.ui.UI;

public class Localizer {
	static MessageSource messageSource;

	public static String getLocalizedTerm(String key) {
		MainUI currentUI = (MainUI) UI.getCurrent();
		ApplicationContext applicationContext = currentUI.getApplicationContext();
		if (messageSource == null) {
			messageSource = applicationContext.getBean(ReloadableResourceBundleMessageSource.class);
		}
		Locale locale = LocaleContextHolder.getLocale();
		if (locale == null) {
			locale = new Locale("en", "CA");
		}
		return messageSource.getMessage(key, null, key, locale);
	}

	public static void setLocale(Locale locale) {
		LocaleContextHolder.setLocale(locale);
	}
}
