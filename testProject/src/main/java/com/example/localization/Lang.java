package com.example.localization;


/* This class is meant to abstract away functionality from Localizer and avoid
 * touching classes that were provided as part of the template */
public class Lang {

	public static String t(String str) {
		return Localizer.getLocalizedTerm(str);
	}
	
}
