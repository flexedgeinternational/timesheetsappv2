package com.example.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.example.localization.Lang;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;

public class GetResource {
	
	public final static String resourceBase = 
		VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + File.separator +
		"VAADIN" + File.separator;
	
	public final static String fileResources = resourceBase + "templates/";
	
	public final static String imageResources = resourceBase + "images/";
	
	/* Getting images */
	public static FileResource barGraph_small(String caption) {
		// Image as a file resource
		FileResource resource = new FileResource(new File(imageResources + "bargraph_small.PNG"));

		// Show the image in the application
		//Image image = new Image(caption, resource);
		
		return resource;
	}
	
		/* Getting templates*/
	public static File getSubmitTimesheetTemplate() {
		return new File(fileResources + "submitTimesheetTemplate.xlsx");
	}
	
	/* change this to use enum */
	public static Map<Integer, String> months() {
		Map<Integer, String> months = new HashMap<Integer, String>();
		
		months.put(1, Lang.t("Jan"));
		months.put(2, Lang.t("Feb"));
		months.put(3, Lang.t("Mar"));
		months.put(4, Lang.t("Apr"));
		months.put(5, Lang.t("May"));
		months.put(6, Lang.t("Jun"));
		months.put(7, Lang.t("July"));
		months.put(8, Lang.t("Aug"));
		months.put(9, Lang.t("Sept"));
		months.put(10, Lang.t("Oct"));
		months.put(11, Lang.t("Nov"));
		months.put(12, Lang.t("Dec"));
		
		return months;
	}
	
	/* change this to use enum */
	public static ArrayList<Integer> holidays() {
		ArrayList<Integer> holidays = new ArrayList<Integer>();
		
		
		
		return holidays;
	}
	
	public static ArrayList<DateValuePair> getDateArrayListFromVal(Long id, int val, double totalVal) {
		/* change to date value pair after testing */
		ArrayList<DateValuePair> elements = new ArrayList<>();
		
		boolean flag = false;
		
		int counter = 0;
		int monthlyVal;
		
		while(true) {
			counter++;
			
			monthlyVal = (int)totalVal - (val * counter);
			/* in case of error stop at 1000 to prevent endless loop */
			if (monthlyVal <= 0 || counter == 1000) {
				monthlyVal = 0;
				flag = true;
			}
			elements.add(
				new DateValuePair((String)addMonths(new java.util.Date(), counter, "string"), monthlyVal)
			);
			if (flag) {
				return elements;
			}
		}
	}
	
	public static Object addMonths(Date date, int counter, String type)	{
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    cal.add(Calendar.MONTH, counter); 
	    
	    if (type.equals("date")) {
	    	return cal.getTime();
	    }
	    
	    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	    String formatted = format.format(cal.getTime());
	    
	    if (type.equals("string")) {
	    	return formatted;
	    }
	    
	    return null;
	}
	
}
