package com.example.util;

public class DateValuePair {
	  private String date;
	  private int value;

	  public DateValuePair(String date, int value) {
	    this.date = date;
	    this.value = value;
	  }

	  public String getDate(){
	    return this.date;
	  }

	  public void setDate(String date){
	    this.date = date;
	  }

	  public int getValue(){
	    return this.value;
	  }

	  public void setValue(int value){
	    this.value = value;
	  }
	}