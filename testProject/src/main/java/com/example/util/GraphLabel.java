package com.example.util;

import com.vaadin.addon.charts.Chart;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class GraphLabel extends VerticalLayout /*implements ClickListener*/ {

	protected Image image;
	protected Chart chart;
	
	public GraphLabel(Image image, Chart chart) {
		this.image = image;
		this.chart = chart;
		this.addComponent(image);
	}

	/*
	@Override
	public void buttonClick(ClickEvent event) {
		// TODO Auto-generated method stub
	}
	*/
}
