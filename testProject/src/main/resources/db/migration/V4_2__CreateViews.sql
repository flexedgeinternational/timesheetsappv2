-- View: public.timesheet_totals

-- DROP VIEW public.timesheet_totals;

CREATE OR REPLACE VIEW public.timesheet_totals AS 
 SELECT timesheet.oid,
    timesheet.end_date,
    timesheet.start_date,
    timesheet.mon_ot,
    timesheet.mon_reg,
    timesheet.tues_ot,
    timesheet.tues_reg,
    timesheet.wed_ot,
    timesheet.wed_reg,
    timesheet.thurs_ot,
    timesheet.thurs_reg,
    timesheet.fri_ot,
    timesheet.fri_reg,
    timesheet.sat_ot,
    timesheet.sat_reg,
    timesheet.sun_ot,
    timesheet.sun_reg,
    timesheet.timesheet_oid,
    timesheet.contract_resource_oid,
    timesheet.submitted,
    sum(timesheet.mon_reg + timesheet.tues_reg + timesheet.wed_reg + timesheet.thurs_reg + timesheet.fri_reg + timesheet.sat_reg + timesheet.sun_reg) AS reg_sum,
    sum(timesheet.mon_ot + timesheet.tues_ot + timesheet.wed_ot + timesheet.thurs_ot + timesheet.fri_ot + timesheet.sat_ot + timesheet.sun_ot) AS ot_sum
   FROM timesheet
  GROUP BY timesheet.oid;
