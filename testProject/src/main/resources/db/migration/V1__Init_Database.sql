CREATE TABLE app_user (
	oid serial primary key,
	username varchar(100) NOT NULL,
	password varchar(100) NOT NULL,
	firstname varchar(100),
	lastname varchar(100),
	email varchar(300),
	active boolean
);

CREATE TABLE role (
	oid serial primary key,
	name varchar(100) NOT NULL
);

CREATE TABLE app_user_role (
	oid serial primary key,
	oid_app_user integer NOT NULL,
	oid_role integer NOT NULL,
	CONSTRAINT fk_oid_app_user FOREIGN KEY (oid_app_user) REFERENCES app_user(oid) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_oid_role FOREIGN KEY (oid_role) REFERENCES role(oid) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE menu_item (
	oid serial primary key,
	fa_icon_name varchar(255),
	menu_title varchar(255),
	menu_view_name varchar(255),
	sort_order integer,
	oid_parent_menu_item integer,
	CONSTRAINT fk_oid_parent_menu_item FOREIGN KEY (oid_parent_menu_item) REFERENCES menu_item(oid)
);

CREATE TABLE menu_item_role (
	oid serial primary key,
	oid_menu_item integer NOT NULL,
	oid_role integer NOT NULL,
	CONSTRAINT fk_oid_menu_item FOREIGN KEY (oid_menu_item) REFERENCES menu_item(oid) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_oid_role FOREIGN KEY (oid_role) REFERENCES role(oid) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE employee (
	oid serial primary key,
	firstname varchar(100) NOT NULL,
	lastname varchar(100) NOT NULL,
	age integer
);

INSERT INTO menu_item (fa_icon_name, menu_title, menu_view_name, sort_order) VALUES ('USER', 'Employees', 'EmployeeListView', 1);

INSERT INTO role (name) VALUES ('SYSADMIN');

INSERT INTO menu_item_role(oid_menu_item, oid_role) VALUES ((Select oid from menu_item where menu_view_name='EmployeeListView'), (Select oid from role where name='SYSADMIN'));

INSERT INTO app_user (active, email, firstname, lastname, password, username) VALUES(true, 'admin@flexedge.com', 'System', 'Administrator', 'sysadmin', 'sysadmin');

INSERT INTO app_user_role (oid_app_user, oid_role)  SELECT u.oid, r.oid FROM app_user u, role r; 



