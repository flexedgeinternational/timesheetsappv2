INSERT INTO menu_item( menu_title, menu_view_name, sort_order) VALUES ('Administration','',2);

INSERT INTO menu_item(fa_icon_name, menu_title,menu_view_name, sort_order, oid_parent_menu_item) VALUES ('FOLDER','Menu Manager','MenuListView',3,(Select oid from menu_item where menu_title='Administration'));
    
INSERT INTO menu_item(fa_icon_name, menu_title,menu_view_name, sort_order, oid_parent_menu_item) VALUES ('EXCLAMATION_TRIANGLE','Security Roles','RoleListView',4,(Select oid from menu_item where menu_title='Administration'));
   
INSERT INTO menu_item(fa_icon_name, menu_title,menu_view_name, sort_order, oid_parent_menu_item) VALUES ('USER','User Manager','AppUserListView',5,(Select oid from menu_item where menu_title='Administration'));

INSERT INTO menu_item_role(oid_menu_item, oid_role) VALUES ((Select oid from menu_item where menu_title='Administration'), (Select oid from role where name='SYSADMIN'));
INSERT INTO menu_item_role(oid_menu_item, oid_role) VALUES ((Select oid from menu_item where menu_title='Menu Manager'), (Select oid from role where name='SYSADMIN'));
INSERT INTO menu_item_role(oid_menu_item, oid_role) VALUES ((Select oid from menu_item where menu_title='Security Roles'), (Select oid from role where name='SYSADMIN'));
INSERT INTO menu_item_role(oid_menu_item, oid_role) VALUES ((Select oid from menu_item where menu_title='User Manager'), (Select oid from role where name='SYSADMIN'));
