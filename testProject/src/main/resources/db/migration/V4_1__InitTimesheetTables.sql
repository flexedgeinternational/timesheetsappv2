insert into currency values('CAD');
insert into currency values('USD');


insert into contract(
	contract_title,
	contract_identifier,
	total_cost,
	currency,
	client,
	service_provider,
	contracting_auth,
	technical_auth,
	vendor_auth,
	status,
	end_date, 
	start_date
)
values(
	'title',
	'1111',
	100000,
	'CAD',
	1,
	1,
	1,
	1,
	1,
	'In Progress',
	current_date,
	current_date + integer '7'
);

insert into contract_resource(contract_oid, resource_oid)
values(1 , 4);

-- Add Companies 
INSERT INTO company(name, street_addr, email, floor, city, province, postal_code, acronym)
values('Numbered', 'Street Addr', 'email1@email1.ca', '1', 'Ottawa', 'ON', 'K1Y123', 'Nu');

INSERT INTO company(name, street_addr, email, floor, city, province, postal_code, acronym)
values('FlexEdge', '123 Avenue', 'business@line.ca', '5th', 'Tampa', 'FL', '4512314', 'FE');

-- Add People
INSERT INTO person(email, province, first_name, last_name, fax_number, work_number, cell_number, company)
values('jayson@numbered.ca', 'ON', 'Jason', 'Last', '613-235-1234', '531-634-2345', '613-531-1235', 1);

INSERT INTO person(email, province, first_name, last_name, fax_number, work_number, cell_number, company)
values('Arnaud@flexedge.com', 'ON', 'Arnaud', 'Bosse', '742 653 3456', '123 452 4632', '542 234 2345', 2);

insert into frequency(frequency) values('Weekly');
insert into frequency(frequency) values('Bi-Weekly');
insert into frequency(frequency) values('Monthly');