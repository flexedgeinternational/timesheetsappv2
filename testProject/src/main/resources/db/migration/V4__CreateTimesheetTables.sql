-- Table: public.currency

-- DROP TABLE public.currency;

CREATE TABLE public.currency
(
  type text NOT NULL,
  CONSTRAINT "PK_CurrencyType" PRIMARY KEY (type)
)
WITH (
  OIDS=FALSE
);
  
-- Table: public.contract

-- DROP TABLE public.contract;

CREATE TABLE public.contract
(
  oid serial, -- Id of a given contract
  contract_title text,
  contract_identifier text, -- Text because we cannot dictate the format that companies use for their contract identifiers ...
  total_cost numeric,
  currency text NOT NULL,
  client integer,
  service_provider integer,
  contracting_auth integer,
  technical_auth integer,
  vendor_auth integer,
  status text,
  end_date timestamp without time zone,
  start_date timestamp without time zone,
  CONSTRAINT "PK_ContractId" PRIMARY KEY (oid),
  CONSTRAINT "FK_ContractCurrency_CurrencyType" FOREIGN KEY (currency)
      REFERENCES public.currency (type) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

-- Table: public.contract_resource

-- DROP TABLE public.contract_resource;

CREATE TABLE public.contract_resource
(
  oid serial,
  contract_oid integer NOT NULL,
  resource_oid integer NOT NULL,
  CONSTRAINT "PK_contract_resource_oid" PRIMARY KEY (oid),
  CONSTRAINT "FK_Contract_Resource_ContractId" FOREIGN KEY (contract_oid)
      REFERENCES public.contract (oid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "FK_Contract_Resource_ResourceId" FOREIGN KEY (resource_oid)
      REFERENCES public.app_user (oid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

-- Constraint: public.app_user_username_unique

-- ALTER TABLE public.app_user DROP CONSTRAINT app_user_username_unique;

ALTER TABLE public.app_user
  ADD CONSTRAINT app_user_username_unique UNIQUE(username);

 
-- Table: public.company

-- DROP TABLE public.company;

CREATE TABLE public.company
(
  oid serial,
  name text NOT NULL,
  street_addr text,
  email text,
  floor text,
  city text,
  province text,
  postal_code text,
  acronym text,
  CONSTRAINT "PK_comany_oid" PRIMARY KEY (oid)
)
WITH (
  OIDS=FALSE
);

-- Table: public.person

-- DROP TABLE public.person;

CREATE TABLE public.person
(
  oid serial,
  email text,
  province text,
  first_name text,
  last_name text,
  fax_number text,
  work_number text,
  cell_number text,
  company integer,
  CONSTRAINT "PK_PeopleOid" PRIMARY KEY (oid),
  CONSTRAINT "FK_person_company_company_oid" FOREIGN KEY (company)
      REFERENCES public.company (oid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);


-- Table: public.file

-- DROP TABLE public.file;

CREATE TABLE public.file
(
  oid serial,
  file bytea,
  CONSTRAINT "PK_file_oid" PRIMARY KEY (oid)
)
WITH (
  OIDS=FALSE
);

-- Table: public.frequency

-- DROP TABLE public.frequency;

CREATE TABLE public.frequency
(
  oid serial,
  frequency text,
  CONSTRAINT "PK_frequency_oid" PRIMARY KEY (oid)
)
WITH (
  OIDS=FALSE
);

-- Table: public.timesheet_template

-- DROP TABLE public.timesheet_template;

CREATE TABLE public.timesheet_template
(
  oid serial,
  name text NOT NULL,
  file integer,
  file_name text,
  _default smallint NOT NULL DEFAULT 0,
  frequency integer,
  CONSTRAINT "PK_TimesheetTemplate_oid" PRIMARY KEY (oid),
  CONSTRAINT "FK_timesheet_template_file_file_oid" FOREIGN KEY (file)
      REFERENCES public.file (oid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "FK_timesheet_template_frequency" FOREIGN KEY (frequency)
      REFERENCES public.frequency (oid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fksr02gkwwau7svgom92bda4j8i FOREIGN KEY (file)
      REFERENCES public.file (oid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

-- Table: public.timesheet

-- DROP TABLE public.timesheet;

CREATE TABLE public.timesheet
(
  oid serial,
  end_date date,
  start_date date,
  mon_ot double precision NOT NULL,
  mon_reg double precision NOT NULL,
  tues_ot double precision NOT NULL,
  tues_reg double precision NOT NULL,
  wed_ot double precision NOT NULL,
  wed_reg double precision NOT NULL,
  thurs_ot double precision NOT NULL,
  thurs_reg double precision NOT NULL,
  fri_ot double precision NOT NULL,
  fri_reg double precision NOT NULL,
  sat_ot double precision NOT NULL,
  sat_reg double precision NOT NULL,
  sun_ot double precision NOT NULL,
  sun_reg double precision NOT NULL,
  timesheet_oid integer,
  contract_resource_oid integer,
  submitted timestamp with time zone,
  CONSTRAINT timesheet_pkey PRIMARY KEY (oid),
  CONSTRAINT "FK_timesheet_contract_resource_oid" FOREIGN KEY (contract_resource_oid)
      REFERENCES public.contract_resource (oid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "FK_timesheet_timesheet_template_oid" FOREIGN KEY (timesheet_oid)
      REFERENCES public.timesheet_template (oid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkbyvwx7c7w3hgkeomij8y07rd FOREIGN KEY (contract_resource_oid)
      REFERENCES public.contract_resource (oid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkowcxj3qc5bppo5qymler8kvxh FOREIGN KEY (timesheet_oid)
      REFERENCES public.timesheet_template (oid) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

